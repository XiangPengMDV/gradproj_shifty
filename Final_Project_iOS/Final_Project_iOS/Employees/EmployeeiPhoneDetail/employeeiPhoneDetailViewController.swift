import UIKit

class employeeiPhoneDetailViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var workIDLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var occupationLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    
    var detailUser = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let URL = detailUser.image {
            imageView.loadImageUsingCache(URL)
        } else {
            imageView.image = UIImage(named: "ShiftyIcon")
        }
        
        workIDLbl.text = detailUser.workID
        nameLbl.text = detailUser.name
        occupationLbl.text = detailUser.occupation
        emailLbl.text = detailUser.email
        phoneLbl.text = detailUser.phone
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
