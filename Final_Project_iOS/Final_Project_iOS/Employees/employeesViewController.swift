

import UIKit
import Firebase

class employeesViewController: UIViewController {
    //iPhone or iPad
    let model = UIDevice.current.model
    // mock master view UI, also collection view for iPhone
    @IBOutlet weak var collectionView: UICollectionView!
    // mock detail view UI, only on iPad, iPhone has a different file
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailWorkID: UILabel!
    @IBOutlet weak var detailName: UILabel!
    @IBOutlet weak var detailOccupation: UILabel!
    @IBOutlet weak var detailEmail: UILabel!
    @IBOutlet weak var detailPhone: UILabel!
    
    @IBOutlet weak var navEdit_outlet: UIBarButtonItem!
    
    var employeesList = [User?]()
    var company_UUID = ""
    
    var _isOnEmployeeEdit = false
    var employeesToDelete : [String] = []
    
    var selectedUser_iPhone = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // collection view dataSource, delegate
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "scheduleiPadWeekDetailCell")
        collectionView.register(UINib(nibName: "employeesListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "employeesListCollectionViewCell")
        
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!

    }
    
    override func viewWillAppear(_ animated: Bool) {
        employeesList.removeAll()
        employeesToDelete.removeAll()
        // get employees list
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User()
                
                user.name = dictionary["name"] as? String
                user.workID = dictionary["workID"] as? String
                user.occupation = dictionary["occupation"] as? String
                user.phone = dictionary["phone"] as? String
                user.email = dictionary["email"] as? String
                user.image = dictionary["image"] as? String
                
                self.employeesList.append(user)
                self.employeesToDelete.append(snapshot.key)
                
                // print(self.employeesList.count)
                
                DispatchQueue.main.async(execute: {
                    self.collectionView?.reloadData()
                })
            }
            
        }, withCancel: nil)
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func navEdit_action(_ sender: Any) {
        if (_isOnEmployeeEdit) {
            self._isOnEmployeeEdit = false
            self.navEdit_outlet.title = "Edit"
            self.collectionView.reloadData()
        } else {
        // edit prompt action sheet
        let editPrompt = UIAlertController(title: "Edit", message: "What are you trying to do?", preferredStyle: .actionSheet)
        // cancel selection
        editPrompt.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            // cancel and do nothing
        }))
        editPrompt.addAction(UIAlertAction(title: "Create New Employee", style: .default, handler: { (action) in
            // new employee
            self.performSegue(withIdentifier: "employeesToCreateNewEmployeeSegue", sender: self)
        }))
        editPrompt.addAction(UIAlertAction(title: "Delete Employee", style: .default, handler: { (action) in
            // delete employee
            self.navEdit_outlet.title = "Done"
            self._isOnEmployeeEdit = true
            self.collectionView.reloadData()
        }))
        
        editPrompt.popoverPresentationController?.sourceView = self.view
        editPrompt.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX*1.9, y: self.view.bounds.midY/2, width: 0, height: 0)
        
        present(editPrompt, animated: true, completion: nil)
        }
    }
    
}// Main Scope





