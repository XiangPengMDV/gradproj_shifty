
import UIKit
import Firebase

class newEmployeeViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    //iPhone or iPad
    let model = UIDevice.current.model
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var occupationInputPickerView: UIPickerView!
    @IBOutlet weak var workIDInput: UITextField!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    let occu = ["Manager", "Employee"]
    
    var selectedOccupation : String!
    var company_UUID = ""
    // keyboard size
    var keyboard_size = CGSize()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        occupationInputPickerView.dataSource = self
        occupationInputPickerView.delegate = self
        nameInput.delegate = self
        workIDInput.delegate = self
        emailInput.delegate = self
        passwordInput.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submit_action(_ sender: Any) {
        if (selectedOccupation == nil || selectedOccupation == "") {
            selectedOccupation = "Manager"
        }
        guard let name = nameInput.text, let workID = workIDInput.text, let email = emailInput.text, let password = passwordInput.text else {
            return
        }
        
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        
        // create new user in Firebase
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
            if (error != nil) {
                return
            }
            let value : [String : Any] = ["name" : name, "occupation": self.selectedOccupation, "email": email, "workID" : workID]
            
            guard let uid = user?.uid else {
                return
            }
            print("THIS \(value), uid \(uid)")
            
            // grab company uuid from users
            let newUserRef = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid)
            newUserRef.updateChildValues(value)
            
            // users branch
            let users_Ref = FIRDatabase.database().reference().child("Users").child(uid)
            let users_Value = ["company-uuid" : self.company_UUID] as [String : Any]
            
            users_Ref.updateChildValues(users_Value)
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOccupation = occu[row]
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return occu[row]
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let height = keyboard_size.height
        if model == "iPad" {
            if (textField == nameInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/4), animated: true)
            } else if (textField == workIDInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/3 + emailInput.frame.height*2), animated: true)
            } else if (textField == emailInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/3 + emailInput.frame.height*3), animated: true)
            } else if (textField == passwordInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/3 + emailInput.frame.height*4), animated: true)
            }
        } else {
            if (textField == nameInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            } else if (textField == workIDInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/4), animated: true)
            } else if (textField == emailInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/4 + emailInput.frame.height), animated: true)
            } else if (textField == passwordInput) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/4 + emailInput.frame.height*2), animated: true)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func dismissKeyboard (_ sender : AnyObject) {
        view.endEditing(true)
    }
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Keyboard size //////////////////////////////////
    func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        keyboard_size = frame.size
    }
    func addObservers(){
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification: notification)
        }
    }
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    ////////////////////////////////////////////////////////////////////////////////
    
}// Main Scope



