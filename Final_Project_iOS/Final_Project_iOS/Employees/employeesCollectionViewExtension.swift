

import Foundation
import UIKit
import Firebase

extension employeesViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "employeesListCollectionViewCell", for: indexPath) as! employeesListCollectionViewCell
        
        // cell Visual Setup ///
        cell.layer.cornerRadius = 11.7
        cell.layer.borderWidth = 2.1
        cell.layer.borderColor = UIColor.purple.cgColor
        
        if (_isOnEmployeeEdit) {
            cell.delete_icon.alpha = 1
        } else {
            cell.delete_icon.alpha = 0
        }
        
        cell.name.text = employeesList[indexPath.row]?.name
        
        if let URL = employeesList[indexPath.row]?.image {
            cell.image.loadImageUsingCache(URL)
        }

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "employeesiPhoneEmployeeListToDetailSegue") {
            let detailVC = segue.destination as! employeeiPhoneDetailViewController
            detailVC.detailUser = selectedUser_iPhone

        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch model {
        case "iPhone":
            if (_isOnEmployeeEdit) {
                let confirmPrompt = UIAlertController(title: "Are you sure", message: "To Remove This Employee?", preferredStyle: .alert)
                let confirm = UIAlertAction(title: "Confirm", style: .default, handler: { (action) in
                    // confirm to delete, perform delete on FireBase, reload data
                    FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(self.employeesToDelete[indexPath.row]).removeValue()
                    self.employeesList.remove(at: indexPath.row)
                    self.collectionView.reloadData()
                })
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                confirmPrompt.addAction(confirm)
                confirmPrompt.addAction(cancel)
                self.present(confirmPrompt, animated: true, completion: nil)
                
            } else {
                self.selectedUser_iPhone = employeesList[indexPath.row]!
                performSegue(withIdentifier: "employeesiPhoneEmployeeListToDetailSegue", sender: self)
            }
            break
        case "iPad":
            if (_isOnEmployeeEdit) {
                let confirmPrompt = UIAlertController(title: "Are you sure", message: "To Remove This Employee?", preferredStyle: .alert)
                let confirm = UIAlertAction(title: "Confirm", style: .default, handler: { (action) in
                    // confirm to delete, perform delete on FireBase, reload data
                    FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(self.employeesToDelete[indexPath.row]).removeValue()
                    self.employeesList.remove(at: indexPath.row)
                    self.collectionView.reloadData()
                })
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                confirmPrompt.addAction(confirm)
                confirmPrompt.addAction(cancel)
                self.present(confirmPrompt, animated: true, completion: nil)
                
            } else {
                detailName.text = employeesList[indexPath.row]?.name
                detailWorkID.text = employeesList[indexPath.row]?.workID
                detailOccupation.text = employeesList[indexPath.row]?.occupation
                detailPhone.text = employeesList[indexPath.row]?.phone
                detailEmail.text = employeesList[indexPath.row]?.email
                
                if let URL = employeesList[indexPath.row]?.image {
                    detailImage.loadImageUsingCache(URL)
                } else {
                    detailImage.image = UIImage(named: "ShiftyIcon")
                }
            }
            break
        default:
            return
        }

    }
    
    
    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/1.15, height: collectionView.frame.height/6.5)
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
    }
    // =========================================================================
    
}// Main Scope



