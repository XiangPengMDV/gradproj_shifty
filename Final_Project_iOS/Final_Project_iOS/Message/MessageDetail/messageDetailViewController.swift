

import UIKit
import Firebase

class messageDetailViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textInput: UITextField!
    
    @IBOutlet weak var inputContainer: UIView!
    
    @IBOutlet weak var navTitle: UINavigationItem!
    
    var selectedUser = User()
    
    var company_UUID = ""
    var receiverID = ""
    
    var chatID = UUID.init().uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(guesture:)))
        scrollView.addGestureRecognizer(tapGesture)
        tableView.dataSource = self
        tableView.delegate = self
        
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        navTitle.title = selectedUser.name
        print(receiverID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
        DispatchQueue.main.async(execute: { () -> Void in
            let scrollPoint = CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height)
            self.tableView.setContentOffset(scrollPoint, animated: true)
        })
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "BACK", style: .done, target: self, action: #selector(self.backToInitial(sender:)))
    }
    @objc func backToInitial(sender: AnyObject) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_SEND(_ sender: UIButton) {
        if (textInput.text! == "" || textInput == nil) {
            let alert = UIAlertController(title: "Empty Message", message: "Make sure you type in something before send", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        } else {
            view.endEditing(true)
            
            print("\(textInput.text!)")
            
            guard let uid = FIRAuth.auth()?.currentUser?.uid else {
                return
            }
            // new chat
            let newChatRef = FIRDatabase.database().reference().child("Users").child(uid).child("chat").child(chatID)
           //setting most recent message
            newChatRef.child("mostRecent").setValue(textInput.text!)
           //setting receiver
            newChatRef.child("receiverName").setValue(selectedUser.name)
          //Messages ref
            let messageId = UUID.init().uuidString
            let messageRef = newChatRef.child("messages").child(messageId)
            messageRef.child("message").setValue(textInput.text!)
            messageRef.child("receiverID").setValue(receiverID)
            
            //Making duplicate for other user
            //new chat
            let receiverChatRef = FIRDatabase.database().reference().child("Users").child(receiverID).child("chat").child(chatID)
            //setting most recent message
            receiverChatRef.child("mostRecent").setValue(textInput.text)
            //setting reciever
            receiverChatRef.child("receiverName").setValue(selectedUser.name)
            //Messages ref
            let receiverMessageRef = receiverChatRef.child("messages").child(messageId)
            receiverMessageRef.child("message").setValue(textInput.text)
            receiverMessageRef.child("receiverID").setValue(receiverID)
            
            textInput.text = ""
        }
    }
    
    ////////////////////////////////////////////////////////////////////
    /////////////////////
    // To move up when //
    // keyboard shows  //
    /////////////////////
    func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        scrollView.setContentOffset(CGPoint(x: 0, y: frame.size.height - inputContainer.frame.height + 4), animated: true)
        
    }
    func keyboardWillHide(notification: Notification) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    @objc func didTapScrollView(guesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    func addObservers(){
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification: notification)
        }
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.keyboardWillHide(notification: notification)
        }
    }
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    ////////////////////////////////////////////////////////////////////
}

extension messageDetailViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = tableView.numberOfRows(inSection: 0)
        if indexPath.row == lastRowIndex - 1 {
            let sections = tableView.numberOfSections
            let rows =  tableView.numberOfRows(inSection: sections - 1)
            if (rows > 0){
                tableView.scrollToRow(at: NSIndexPath.init(row: rows - 1, section: sections - 1) as IndexPath, at: .bottom, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageDetailTableViewCell", for: indexPath) as! messageDetailTableViewCell
        
        cell.messageTextSubview.text = indexPath.row.description
        if (indexPath.row % 2 == 0) {
            cell.messageTextSubview.textAlignment = .right
            cell.messageTextSubview.text = "Sender \(indexPath.row)"
            
            cell.messageTextContent.textAlignment = .right
            
        } else {
            cell.messageTextSubview.textAlignment = .left
            cell.messageTextSubview.text = "Receiver \(indexPath.row)"
            
            cell.messageTextContent.textAlignment = .left
        }
        return cell
    }
}








