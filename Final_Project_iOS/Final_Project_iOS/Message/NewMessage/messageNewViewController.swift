

import UIKit
import Firebase

class messageNewViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var employeesList = [User?]()
    var company_UUID = ""
    var selectedUser = User()
    var selectedUserID = ""
    var temp_ids : [String] = []

    @IBOutlet weak var collectionView: UICollectionView!
    //messageNewListCollectionViewCell
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "employeesListCollectionViewCell")
        collectionView.register(UINib(nibName: "employeesListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "employeesListCollectionViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        employeesList.removeAll()
        // get employees list
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User()
                
                user.name = dictionary["name"] as? String
                user.workID = dictionary["workID"] as? String
                user.occupation = dictionary["occupation"] as? String
                user.phone = dictionary["phone"] as? String
                user.email = dictionary["email"] as? String
                user.image = dictionary["image"] as? String
                
                self.employeesList.append(user)
                self.temp_ids.append(snapshot.key)

                DispatchQueue.main.async(execute: {
                    self.collectionView?.reloadData()
                })
            }
        }, withCancel: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////////////////////////////////////////////////////////////////
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeesList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "employeesListCollectionViewCell", for: indexPath) as! employeesListCollectionViewCell
        
        cell.delete_icon.alpha = 0
        
        cell.name.text = employeesList[indexPath.row]?.name
        
        if let URL = employeesList[indexPath.row]?.image {
            cell.image.loadImageUsingCache(URL)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedUser = employeesList[indexPath.row]!
        selectedUserID = temp_ids[indexPath.row]
        performSegue(withIdentifier: "segueMessageNewToDetail", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueMessageNewToDetail") {
            let VC = segue.destination as! messageDetailViewController
            VC.selectedUser = selectedUser
            VC.receiverID = selectedUserID
        }
    }
    ////////////////////////////////////////////////////////////////
    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/1.15, height: collectionView.frame.height/6.5)
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
    }
    // =========================================================================
}
