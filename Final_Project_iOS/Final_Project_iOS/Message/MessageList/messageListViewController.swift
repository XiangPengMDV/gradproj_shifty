
import UIKit
import Firebase

class messageListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var notificationList : [NotificationItem] = []
    var noti_id_list : [String] = []
    var company_UUID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 75
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        notificationList.removeAll()
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid).child("notifications").observe(.childAdded, with: { (snap) in
            // get all notification
            let noti = NotificationItem()
            if let dict = snap.value as? [String : AnyObject] {
                noti.text = dict["notificationText"] as? String
                noti.read = dict["read"] as? Bool
                
                self.notificationList.append(noti)
                self.noti_id_list.append(snap.key)
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            }
            
        }, withCancel: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func action_newMessage(_ sender: UIBarButtonItem) {
//        performSegue(withIdentifier: "segueMessageToNewList", sender: self)
//    }

}

extension messageListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // when user select this notification (cell), mark as "READ"
        // reloadData change the icon (async?)
        let noti_id = noti_id_list[indexPath.row]
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let noti_ref = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid).child("notifications").child(noti_id)
        noti_ref.child("read").setValue(true)
        notificationList[indexPath.row].read = true
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (notificationList.count > 0) {
            return notificationList.count
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageListTableViewCell", for: indexPath) as! messageListTableViewCell
        // cell image - indicator Notification Unread/Read
        // cell name - main Notification text content
        if (notificationList[indexPath.row].read!) {
            cell.cellImage.image = UIImage(named: "NotificationRead")
        } else {
            cell.cellImage.image = UIImage(named: "NotificationOn")
        }
        cell.cellName.text = notificationList[indexPath.row].text
        
        return cell
    }
}



