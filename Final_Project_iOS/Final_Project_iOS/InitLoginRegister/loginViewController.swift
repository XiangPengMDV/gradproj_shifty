import UIKit
import Firebase

class loginViewController: UIViewController, UITextFieldDelegate {
    //iPhone or iPad
    let model = UIDevice.current.model
    // scrollView
    @IBOutlet weak var scrollView: UIScrollView!
    // UI
    @IBOutlet weak var email_input: UITextField!
    @IBOutlet weak var password_input: UITextField!
    
    override func viewDidAppear(_ animated: Bool) {
        // auto-login
        if (FIRAuth.auth()?.currentUser) != nil{
            self.performSegue(withIdentifier: "loginToMain", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add tapGuesture for scrollView, textField Delegate
        email_input.delegate = self
        password_input.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // login button action
    @IBAction func login_action(_ sender: Any) {
        // get input from textField
        let email = email_input.text
        let password = password_input.text
        // firebase auth
        FIRAuth.auth()?.signIn(withEmail: email!, password: password!, completion: { (user, error) in
            if (error == nil) {
                // everything is fine, go to main
                self.performSegue(withIdentifier: "loginToMain", sender: self)
            } else {
                // print out the error
                let alert = UIAlertController(title: "Login Error", message: "Invalid Emmail or Password", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            }
        })
    }// _end login_action
    
    // register button action
    @IBAction func register_action(_ sender: Any) {
        performSegue(withIdentifier: "loginToRegister", sender: self)
    }
    // forgot password button action
    @IBAction func forgotPassword_action(_ sender: Any) {
        // create alert
        let alert = UIAlertController(title: "Forgot Your Password?", message: "Please enter your email so we can send you a reset password email", preferredStyle: .alert)
        // add text input to it
        alert.addTextField { (textField) in
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        // cancel
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        // send (OK)
        let send = UIAlertAction(title: "Send", style: .default) { (action) in
            // get text input from alert and store it as string
            let textInput = alert.textFields![0] as UITextField
            let emailString = textInput.text
            // input validation
            if (emailString == nil || emailString == "") {
                // show error
                let alert = UIAlertController(title: "Oops!", message: "Invalid Email", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                // else just send reset
                FIRAuth.auth()?.sendPasswordReset(withEmail: emailString!, completion: { (error) in
                    var title = ""
                    var message = ""
                    // if error or not, tell the user whats going no
                    if error != nil {
                        title = "Error!"
                        message = (error?.localizedDescription)!
                    } else {
                        title = "Success!"
                        message = "Password reset email sent."
                    }
                    // show the result
                    let resultAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    let resultOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    resultAlert.addAction(resultOK)
                    
                    self.present(resultAlert, animated: true, completion: nil)
                })
            }
        }
        alert.addAction(cancel)
        alert.addAction(send)
        // present it
        self.present(alert, animated: true, completion: nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let height = self.view.frame.height
        if model == "iPad" {
            if (textField == email_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/6), animated: true)
            } else if (textField == password_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/6 + password_input.frame.height), animated: true)
            } else {}
        } else {
            if (textField == email_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: 120), animated: true)
            } else if (textField == password_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
            } else {}
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func dismissKeyboard (_ sender : AnyObject) {
        view.endEditing(true)
    }
    ////////////////////////////////////////////////////////////////////////////////
   
}// Main Scope
