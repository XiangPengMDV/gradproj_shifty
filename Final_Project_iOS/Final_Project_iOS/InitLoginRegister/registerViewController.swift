import UIKit
import Firebase

class registerViewController: UIViewController, UITextFieldDelegate {
    //iPhone or iPad
    let model = UIDevice.current.model
    // scrollView
    @IBOutlet weak var scrollView: UIScrollView!
    // UI
    @IBOutlet weak var fullName_input: UITextField!
    @IBOutlet weak var companyName_input: UITextField!
    @IBOutlet weak var email_input: UITextField!
    @IBOutlet weak var password_input: UITextField!
    @IBOutlet weak var confirmPW_input: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // add tapGuesture for scrollView, textField Delegate
        fullName_input.delegate = self
        companyName_input.delegate = self
        email_input.delegate = self
        password_input.delegate = self
        confirmPW_input.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //cancel
    @IBAction func cancel_action(_ sender: Any) {
        // dismiss the view
        self.dismiss(animated: true, completion: nil)
    }
    // continue
    @IBAction func continue_action(_ sender: Any) {
        // get all input here
        let fullName = self.fullName_input.text
        let companyName = self.companyName_input.text
        let email = self.email_input.text
        let password = self.password_input.text
        let confirmPW = self.confirmPW_input.text
        // input empty check, validation
        if (fullName == "" || companyName == "" || email == "" || password == "" || confirmPW == "") {
            // empty input
            let alert = UIAlertController(title: "Empty Fields", message: "Please Fill In All Information", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        } else {
            // no empty fields, now check for validation
            if (!(email?.contains("@"))! || !(email?.contains("."))!) {
                // check email validation
                let alert = UIAlertController(title: "Wrong Email", message: "Please Double Check Your Email", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                // email good, check password inputs
                if (password != confirmPW) {
                    // confirm password
                    let alert = UIAlertController(title: "Wrong Password", message: "Please Make Sure You Enter The Same Password", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    // all good, create new user
                    FIRAuth.auth()?.createUser(withEmail: email!, password: password!, completion: { (user, error) in
                        if (error == nil) {
                            // user UUID
                            let userUUID = user?.uid
                            // company UUID
                            let company_UUID = UUID.init().uuidString
                            // companies branch
                            let company_Ref = FIRDatabase.database().reference().child("Companies").child(company_UUID)
                            let company_Value = ["company-name" : companyName!] as [String : Any]
                            company_Ref.updateChildValues(company_Value)
                            // employees branch
                            let employee_Ref = company_Ref.child("employees").child(userUUID!)
                            let employee_Value = ["email" : email!, "name" : fullName!, "occupation" : "Boss"] as [String : Any]
                            employee_Ref.updateChildValues(employee_Value)
                            // users branch
                            let users_Ref = FIRDatabase.database().reference().child("Users").child(userUUID!)
                            let users_Value = ["company-uuid" : company_UUID] as [String : Any]
                            
                            users_Ref.updateChildValues(users_Value)
                            // dismiss after
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            // print out the error
                            let errorText = error?.localizedDescription
                            let alert = UIAlertController(title: "Register Error", message: "\(errorText!)", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                } // _end password check
            } // _end email check
        }// _end empty check
        // print("continue")
    }// _end continue
    
    ////////////////////////////////////////////////////////////////////////////////
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let height = self.view.frame.height
        if model == "iPad" {
            if (textField == fullName_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            } else if (textField == companyName_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: height/6), animated: true)
            } else if (textField == email_input) {
                                scrollView.setContentOffset(CGPoint(x: 0, y: height/6 + password_input.frame.height), animated: true)
            } else if (textField == password_input) {
                                scrollView.setContentOffset(CGPoint(x: 0, y: height/6 + password_input.frame.height*2), animated: true)
            } else if (textField == confirmPW_input) {
                                scrollView.setContentOffset(CGPoint(x: 0, y: height/6 + password_input.frame.height*3), animated: true)
            }
        } else {
            if (textField == email_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: 120), animated: true)
            } else if (textField == password_input) {
                scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
            } else {}
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func dismissKeyboard (_ sender : AnyObject) {
        view.endEditing(true)
    }
    ////////////////////////////////////////////////////////////////////////////////
    
}// Main Scope


