

import UIKit

class announcementDetailiPhoneViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var ancmtTitle: UILabel!
    @IBOutlet weak var dateAuthor: UILabel!
    @IBOutlet weak var contentText: UITextView!
    
    @IBOutlet weak var commentCollectionview: UICollectionView!
    
    var annouce = Announcement()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        commentCollectionview.dataSource = self
        commentCollectionview.delegate = self
        
        commentCollectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "annoucemnetiPhoneCommentCell")
        commentCollectionview.register(UINib(nibName: "announcementCommentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "annoucemnetiPhoneCommentCell")
        
        ancmtTitle.text = annouce.title
        let date_author = "\(annouce.date!) - Posted By: \(annouce.author!)"
        dateAuthor.text = date_author
        contentText.text = annouce.body
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "annoucemnetiPhoneCommentCell", for: indexPath)
        
        return cell
    }

    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/1.15, height: collectionView.frame.height/6.5)
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
    }
    // =========================================================================
}
