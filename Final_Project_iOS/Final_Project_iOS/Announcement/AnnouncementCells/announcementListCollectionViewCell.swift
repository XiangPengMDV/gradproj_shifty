//
//  announcementListCollectionViewCell.swift
//  Final_Project_iOS
//
//  Created by Xiang Peng on 10/29/17.
//  Copyright © 2017 Xiang Peng. All rights reserved.
//

import UIKit

class announcementListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
