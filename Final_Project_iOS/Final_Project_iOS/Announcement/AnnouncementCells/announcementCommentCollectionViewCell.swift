//
//  announcementCommentCollectionViewCell.swift
//  Final_Project_iOS
//
//  Created by Xiang Peng on 10/29/17.
//  Copyright © 2017 Xiang Peng. All rights reserved.
//

import UIKit

class announcementCommentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
