

import Foundation
import UIKit
import Firebase

extension announcementViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        // announcement collection list
        case announcementCollectionView:
            return announcementList.count
        // comment in iPad aside
        case commentCollectionView:
            if (commentList.count > 0) {
                return commentList.count
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        // announcement collection list
        case announcementCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "announcementListCell", for: indexPath) as! announcementListCollectionViewCell
            // cell Visual Setup ///
            cell.layer.cornerRadius = 11.7
            cell.layer.borderWidth = 2.1
            cell.layer.borderColor = UIColor.purple.cgColor
            
            cell.title.text = announcementList[indexPath.row].title
            cell.date.text = announcementList[indexPath.row].date
            return cell
        // comment in iPad aside
        case commentCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "annoucemnetiPadCommentCell", for: indexPath) as! announcementCommentCollectionViewCell
            // cell Visual Setup ///
            cell.layer.cornerRadius = 11.7
            cell.layer.borderWidth = 2.1
            cell.layer.borderColor = UIColor.purple.cgColor
            if (commentList.count > 0) {
                cell.name.text = commentList[indexPath.row].name
                cell.date.text = commentList[indexPath.row].date
                cell.comment.text = commentList[indexPath.row].comment
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch model {
        case "iPad":
            a_title.text = announcementList[indexPath.row].title
            let date_author = "\(announcementList[indexPath.row].date!) - Posted By: \(announcementList[indexPath.row].author!)"
            a_date_author.text = date_author
            a_content.text = announcementList[indexPath.row].body
            
            self.commentCollectionView.reloadData()
            self.commentList.removeAll()
            FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("announcements").child(announcementList[indexPath.row].uuid!).child("comments").observe(.childAdded, with: { (snap) in
                if let dict = snap.value as? [String : AnyObject] {
                    let _comment = Comment()
                    _comment.name = dict["name"] as? String
                    _comment.date = dict["date"] as? String
                    _comment.comment = dict["comment"] as? String
                    self.commentList.append(_comment)
                    print(self.commentList.count)
                    self.commentCollectionView.reloadData()
                }
            }, withCancel: nil)
            break
        case "iPhone":
            self.selectedPost = indexPath.row
            self.performSegue(withIdentifier: "announcementToDetailiPhoneSegue", sender: self)
            break
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "announcementToDetailiPhoneSegue") {
        let dest = segue.destination as! announcementDetailiPhoneViewController
        dest.annouce = announcementList[self.selectedPost]
        }
    }
    
    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == commentCollectionView) {
            return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height/3)
        } else if (collectionView == announcementCollectionView) {
            return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height/7)
        } else {
            return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height/6)
        }
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
    }
    // =========================================================================
}



