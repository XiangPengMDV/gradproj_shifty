
import UIKit
import Firebase

class newAnnouncementViewController: UIViewController {
    
    @IBOutlet weak var _title: UITextField!
    @IBOutlet weak var _content: UITextView!

    var company_UUID = ""
    var date = ""
    var _announcement = Announcement()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        FIRDatabase.database().reference().child("Companies").child(company_UUID).child("employees").child(uid).observe(.value, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
                self._announcement.author = (dict["name"] as? String)!
            }
        }, withCancel: nil)
        let timestamp = Int(Date().timeIntervalSince1970) as NSNumber
        _announcement.uuid = UUID.init().uuidString
        _announcement.date = _announcement.timeStampFormat(timestamp)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func submit_action(_ sender: Any) {
        _announcement.title = _title.text
        _announcement.body = _content.text
        
        let value : [String : Any] = ["author" : _announcement.author!, "body": _announcement.body!, "date": _announcement.date!, "title" : _announcement.title!]
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("announcements").child(self._announcement.uuid!).updateChildValues(value)
        self.navigationController?.popViewController(animated: true)
    }
    

}
