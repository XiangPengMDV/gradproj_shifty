

import UIKit
import Firebase

class announcementViewController: UIViewController {
    //iPhone or iPad
    let model = UIDevice.current.model
    @IBOutlet weak var announcementCollectionView: UICollectionView!
    @IBOutlet weak var commentCollectionView: UICollectionView!
    
    @IBOutlet weak var a_title: UILabel!
    @IBOutlet weak var a_date_author: UILabel!
    @IBOutlet weak var a_content: UITextView!
    
    var company_UUID = ""
    var comment_uuid = ""
    
    var announcementList = [Announcement]()
    var commentList = [Comment]()
        var selectedPost = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        announcementCollectionView.dataSource = self
        announcementCollectionView.delegate = self
        
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        switch model {
        case "iPad":
            announcementCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "announcementListCell")
            announcementCollectionView.register(UINib(nibName: "announcementListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "announcementListCell")
            commentCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "annoucemnetiPadCommentCell")
            commentCollectionView.register(UINib(nibName: "announcementCommentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "annoucemnetiPadCommentCell")
            commentCollectionView.dataSource = self
            commentCollectionView.delegate = self
            break
        case "iPhone":
            announcementCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "announcementListCell")
            announcementCollectionView.register(UINib(nibName: "announcementListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "announcementListCell")
            break
        default:
            break
        }
        
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("announcements").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let _announcement = Announcement()
                
                _announcement.author = dictionary["author"] as? String
                _announcement.body = dictionary["body"] as? String
                _announcement.date = dictionary["date"] as? String
                _announcement.title = dictionary["title"] as? String
                _announcement.uuid = snapshot.key
                
                self.announcementList.append(_announcement)
                
                DispatchQueue.main.async(execute: {
                    self.announcementCollectionView.reloadData()
                })
                
            }
            
        }, withCancel: nil)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        announcementCollectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func edit_action(_ sender: Any) {
        // edit prompt action sheet
        let editPrompt = UIAlertController(title: "Edit", message: "What are you trying to do?", preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let newAnno = UIAlertAction(title: "New Announcement", style: .default) { (action) in
            // new announcement
            self.performSegue(withIdentifier: "announcementToNewAnnouncementSegue", sender: self)
        }
        let editAnno = UIAlertAction(title: "Edit Announcement", style: .default) { (action) in
            // edit announcement
        }
        
        editPrompt.addAction(newAnno)
        editPrompt.addAction(editAnno)
        
        if (model == "iPad") {
            // iPad only, put comment edit together
            let newComm = UIAlertAction(title: "New Comment", style: .default, handler: { (action) in
                // new comment
            })
            let editComm = UIAlertAction(title: "Edit Comment", style: .default, handler: { (action) in
                // edit comment
            })
            editPrompt.addAction(newComm)
            editPrompt.addAction(editComm)
        }
        editPrompt.addAction(cancel)
        
        editPrompt.popoverPresentationController?.sourceView = self.view
        editPrompt.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX*1.9, y: self.view.bounds.midY/2, width: 0, height: 0)
        
        present(editPrompt, animated: true, completion: nil)
    }
    
}
