

import Foundation
import UIKit

class User {
    var image : String?
    var name : String?
    var workID : String?
    var occupation : String?
    var phone : String?
    var email : String?
    
    var sunday : Bool?
    var monday : Bool?
    var tuesday : Bool?
    var wednesday : Bool?
    var thursday : Bool?
    var friday : Bool?
    var saturday : Bool?
}

class NotificationItem {
    var text : String?
    var read : Bool?
}

struct UserDefaultsKeys {
    static let key_companyUUID = "userDefaults_companyUUID"
    static let key_deviceModel = "userDefaults_deviceMoodel"
}

class ScheduleDayDetail {
    var image : String?
    var name : String?
    var startTime : String?
    var endTime : String?
}

class Announcement {
    var author : String?
    var body : String?
    var date : String?
    var title : String?
    var uuid : String?
    
    var _timeStamp: NSNumber?
    
    func timeStampFormat(_ stamp: NSNumber?) -> String {
        //Pulling timestamp
        if let seconds = stamp?.doubleValue {
            let timeStampDate = Date(timeIntervalSince1970: seconds)
            //Formatting the date
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "MM-dd-yyyy"
            return dateFormater.string(from: timeStampDate)
        } else {
            return "N/A"
        }
    }
}

class Comment {
    var comment : String?
    var date : String?
    var name : String?
}

//MARK: IMAGE EXTENSION FOR CACHING AND DATA SAVING
//Used when caching images
let imageCache = NSCache<AnyObject, UIImage>()
extension UIImageView {
    func loadImageUsingCache(_ urlString: String) {
        //check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) {
            self.image = cachedImage
            return
        }
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler:  { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    self.image = downloadedImage
                }
            })
        }).resume()
    }
}// _end extension UIImageView


extension Date {
    struct Calendar {
        static let gregorian = Foundation.Calendar(identifier: .gregorian)
    }

    var startOfWeek: Date {
        return Calendar.gregorian.date(from: (Calendar.gregorian).dateComponents([.yearForWeekOfYear, .weekOfYear ], from: self))!
    }
}






