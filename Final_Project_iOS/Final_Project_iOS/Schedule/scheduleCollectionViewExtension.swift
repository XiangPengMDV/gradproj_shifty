import Foundation
import UIKit
import Firebase

extension scheduleViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (collectionView == iPhoneScheduleWeekCollectionView) {
            return 7
        } else {
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        // iPad Time Off Request
        case iPadScheduleTimeOffRequestCollectionView:
            return 7
        // iPad Schedule Week Detail
        case iPadScheduleWeekDayDetailCollectionView:
            if (iPadSelectedDateScheduleEmployeeList.count == 0) {
                return 0
            } else {
                return iPadSelectedDateScheduleEmployeeList.count
            }
        // iPhone Schedule Week List
        case iPhoneScheduleWeekCollectionView:
            if (dayDetailEmployeeList.count > 0) {
                return dayDetailEmployeeList[section].count
            } else {
                return 0
            }
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // scheduleCellPhone  scheduleiPadTimeOffRequestCell   scheduleiPadWeekDetailCell
        
        var cellIdentifier = ""
        switch collectionView {
        // iPad Time Off Request
        case iPadScheduleTimeOffRequestCollectionView:
            cellIdentifier = "scheduleiPadTimeOffRequestCell"
            break
        // iPad Schedule Week Detail
        case iPadScheduleWeekDayDetailCollectionView:
            cellIdentifier = "scheduleiPadWeekDetailCell"
            break
        // iPhone Schedule Week List
        case iPhoneScheduleWeekCollectionView:
            cellIdentifier = "scheduleCellPhone" //"scheduleCellPhone"
            break
        default:
            break
        }
        
        switch collectionView {
        // iPad Time Off Request
        case iPadScheduleTimeOffRequestCollectionView:
            let requestCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
            
            return requestCell
        // iPad Schedule Week Detail
        case iPadScheduleWeekDayDetailCollectionView:
            let weekDayCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! scheduleWeekDayCollectionViewCell
            
            // cell Visual Setup ///
            weekDayCell.layer.cornerRadius = 11.7
            weekDayCell.layer.borderWidth = 2.1
            weekDayCell.layer.borderColor = UIColor.purple.cgColor
            
            weekDayCell.name.text = iPadSelectedDateScheduleEmployeeList[indexPath.row]?.name
            let timeString = "\((iPadSelectedDateScheduleEmployeeList[indexPath.row]?.startTime)!) - \((iPadSelectedDateScheduleEmployeeList[indexPath.row]?.endTime)!)"
            weekDayCell.time.text = timeString
            
            if let URL = iPadSelectedDateScheduleEmployeeList[indexPath.row]?.image {
                weekDayCell.image.loadImageUsingCache(URL)
            }
            
            if (_isScheduleOnDelete) {
                weekDayCell.delete_icon.alpha = 1.0
            } else {
                weekDayCell.delete_icon.alpha = 0.0
            }
            
            return weekDayCell
        // iPhone Schedule Week List
        case iPhoneScheduleWeekCollectionView:
            let weekCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! scheduleWeekDayCollectionViewCell
            
            // cell Visual Setup ///
            weekCell.layer.cornerRadius = 11.7
            weekCell.layer.borderWidth = 2.1
            weekCell.layer.borderColor = UIColor.purple.cgColor
            
//                        weekCell.weekday.text = WeekDayArray[indexPath.row]
//                        weekCell.date.text = dateArray[indexPath.row]
                        weekCell.name.text = dayDetailEmployeeList[indexPath.section][indexPath.row]?.name
                        let timeString = "\((dayDetailEmployeeList[indexPath.section][indexPath.row]?.startTime)!) - \((dayDetailEmployeeList[indexPath.section][indexPath.row]?.endTime)!)"
                        weekCell.time.text = timeString
            
                        if let URL = dayDetailEmployeeList[indexPath.section][indexPath.row]?.image {
                            weekCell.image.loadImageUsingCache(URL)
                        }
            
            // print("\(dayDetailEmployeeList[indexPath.section][indexPath.row]?.name!)")
            
            weekCell.delete_icon.alpha = 0.0
            
            return weekCell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
            return cell
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "scheduleiPhoneWeekToDayDetailSegue" {
            // print(WeekDayArray[indexPath_Row])
            let detailVC = segue.destination as! scheduleWeekDayViewController
            detailVC.titleText = WeekDayArray[indexPath_Row]
            detailVC.date = dateArray[indexPath_Row]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                if (collectionView == iPadScheduleWeekDayDetailCollectionView) {
                    if (_isScheduleOnDelete) {
                        // delete
                        let confirmPrompt = UIAlertController(title: "Are you sure", message: "To Remove This Employee?", preferredStyle: .alert)
                        let confirm = UIAlertAction(title: "Confirm", style: .default, handler: { (action) in
                            // confirm to delete, perform delete on FireBase, reload data
                            FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("schedule").child(self._dateStringForDelete).child(self._employeesToDelete[indexPath.row]).removeValue()
                            self.iPadSelectedDateScheduleEmployeeList.remove(at: indexPath.row)

                            // send notification here
                            // need : receiver uuid, dateString
                            let noti_text = "ou have been deleted from the schedule for \(self._dateStringForDelete)"
                            let noti_uuid = UUID.init().uuidString
                            let notiRef = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(self.noti_user_uuid_list[indexPath.row]).child("notifications").child(noti_uuid)
                            let noti_value : [String : Any] = ["notificationText" : noti_text, "notified" : false, "read" : false]
                            notiRef.updateChildValues(noti_value)
                            
                            self.iPadScheduleWeekDayDetailCollectionView.reloadData()
                        })
                        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                        confirmPrompt.addAction(confirm)
                        confirmPrompt.addAction(cancel)
                        self.present(confirmPrompt, animated: true, completion: nil)
                    } else {
                        // regular
                    }
                }
    }
    
    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if (collectionView == iPhoneScheduleWeekCollectionView) {
            return 4
        } else {
            return 20
        }
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == iPhoneScheduleWeekCollectionView) {
            return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height/10)
        } else {
            return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height/6)
        }
        
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if (collectionView == iPhoneScheduleWeekCollectionView) {
            return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        } else {
            return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
        }
        
    }
    // =========================================================================
    
    // collectionView header
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "scheduleCollectionViewHeaderiPhone", for: indexPath) as! scheduleHeaderCollectionReusableView
        header.weekDayLabel.text = WeekDayArray[indexPath.section]
        header.layer.borderColor = UIColor.purple.cgColor
        header.layer.borderWidth = 3.1
        return header
    }
    
}
