
import UIKit
import Firebase

class scheduleAddNewViewController: UIViewController {
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var startTimePicker: UIDatePicker!
    @IBOutlet weak var endTimePicker: UIDatePicker!
    
    var employeesList = [User?]()
    var user_uuid_list : [String] = []
    var selectedEmployee = User()
    var selectedEmployee_UUID = ""
    var selectedDate = ""
    var company_UUID = ""

    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "scheduleAddNewListCell")
        collectionView.register(UINib(nibName: "scheduleAddNewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "scheduleAddNewListCell")
        
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        // get employees list
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User()
                
                user.image = dictionary["image"] as? String
                user.name = dictionary["name"] as? String
                user.workID = dictionary["workId"] as? String
                user.occupation = dictionary["occupation"] as? String
                user.phone = dictionary["phone"] as? String
                user.email = dictionary["email"] as? String
                
                user.sunday = dictionary["Sunday"] as? Bool
                user.monday = dictionary["Monday"] as? Bool
                user.tuesday = dictionary["Tuesday"] as? Bool
                user.wednesday = dictionary["Wednesday"] as? Bool
                user.thursday = dictionary["Thursday"] as? Bool
                user.friday = dictionary["Friday"] as? Bool
                user.saturday = dictionary["Saturday"] as? Bool
                
                self.employeesList.append(user)
                self.user_uuid_list.append(snapshot.key)
                
                // print(self.employeesList.count)
                
                DispatchQueue.main.async(execute: {
                    self.collectionView?.reloadData()
                })
                
            }
        }, withCancel: nil)
        
        dateFormatter.dateFormat = "HH:mm"
        let presetDate = dateFormatter.date(from: "17:00")
        startTimePicker.date = presetDate!
        endTimePicker.date = presetDate!
        
    } // _end viewDidLoad()

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submit_action(_ sender: Any) {
        
        let start = dateFormatter.string(from: startTimePicker.date)
        let end = dateFormatter.string(from: endTimePicker.date)
    
        var image_url = "placeHolder"
        if let this_url = selectedEmployee.image {
            image_url = this_url
        }
        let uuid = UUID.init().uuidString
        let ref = FIRDatabase.database().reference().child("Companies").child(company_UUID).child("schedule").child(selectedDate).child(uuid)

        let values : [String : Any] = ["name" : self.selectedEmployee.name!, "schedule-key" : uuid, "shift-start" : start, "shift-end" : end, "image" : image_url, "uuid" : selectedEmployee_UUID]
        ref.updateChildValues(values)
        
        // send out a notification here
        // need : receiver uuid, dateString
        let noti_text = "ou have been added to the schedule for \(selectedDate)"
        let noti_uuid = UUID.init().uuidString
        let notiRef = FIRDatabase.database().reference().child("Companies").child(company_UUID).child("employees").child(selectedEmployee_UUID).child("notifications").child(noti_uuid)
        let noti_value : [String : Any] = ["notificationText" : noti_text, "notified" : false, "read" : false]
        notiRef.updateChildValues(noti_value)
        
        self.navigationController?.popToRootViewController(animated: true)
    }

}
