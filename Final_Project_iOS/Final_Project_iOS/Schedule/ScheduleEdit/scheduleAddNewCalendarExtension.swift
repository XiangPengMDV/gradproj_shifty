
import Foundation

extension scheduleAddNewViewController : FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // get year, month, day, weekday here
        let _date = date.description.prefix(10).components(separatedBy: "-")
        let year = _date[0]
        let month = _date[1]
        let day = _date[2]
        // month-day-year for the firebase fetch
        let dateString = "\(month)-\(day)-\(year)"
        selectedDate = dateString
    }
}
