
import Foundation

extension scheduleAddNewViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scheduleAddNewListCell", for: indexPath) as! scheduleAddNewCollectionViewCell
        
        // cell Visual Setup ///
//        cell.layer.cornerRadius = 11.7
//        cell.layer.borderWidth = 2.1
//        cell.layer.borderColor = UIColor.purple.cgColor
        
        cell.name.text = employeesList[indexPath.row]?.name
        
        if let URL = employeesList[indexPath.row]?.image {
            cell.image.loadImageUsingCache(URL)
        }
        
        let normalColor = UIColor(red: 197/255, green: 255/255, blue: 240/255, alpha: 1.0)
        let selectColor = UIColor(red: 98/255, green: 230/255, blue: 255/255, alpha: 1.0)
        
        var daysOff : [Bool] = []
       
        if (employeesList[indexPath.row]?.monday != nil) {
            daysOff.append((employeesList[indexPath.row]?.monday)!)
        } else {
            daysOff.append(false)
        }
        if (employeesList[indexPath.row]?.tuesday != nil) {
            daysOff.append((employeesList[indexPath.row]?.tuesday)!)
        } else {
            daysOff.append(false)
        }
        if (employeesList[indexPath.row]?.wednesday != nil) {
            daysOff.append((employeesList[indexPath.row]?.wednesday)!)
        } else {
            daysOff.append(false)
        }
        if (employeesList[indexPath.row]?.thursday != nil) {
            daysOff.append((employeesList[indexPath.row]?.thursday)!)
        } else {
            daysOff.append(false)
        }
        if (employeesList[indexPath.row]?.friday != nil) {
            daysOff.append((employeesList[indexPath.row]?.friday)!)
        } else {
            daysOff.append(false)
        }
        if (employeesList[indexPath.row]?.saturday != nil) {
            daysOff.append((employeesList[indexPath.row]?.saturday)!)
        } else {
            daysOff.append(false)
        }
        if (employeesList[indexPath.row]?.sunday != nil) {
            daysOff.append((employeesList[indexPath.row]?.sunday)!)
        } else {
            daysOff.append(false)
        }
        
        for day in cell.lblWeekDays {
             print("day \(day.tag) -- \(daysOff[day.tag - 1])")
            if (daysOff[day.tag - 1]) {
                day.backgroundColor = selectColor
                day.addSubview(UIImageView.init(image: UIImage(named: "CancelIcon")))
            } else {
                day.backgroundColor = normalColor
            }
        }
        
        daysOff.removeAll()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.backgroundColor = UIColor.lightGray.cgColor
        selectedEmployee = employeesList[indexPath.row]!
        selectedEmployee_UUID = user_uuid_list[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.backgroundColor = UIColor.clear.cgColor
    }
    
    
    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/1.05, height: collectionView.frame.height/6.5)
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    // =========================================================================
}


