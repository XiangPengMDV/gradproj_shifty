
import UIKit
import Firebase

class scheduleViewController: UIViewController {
    //iPhone or iPad
    let model = UIDevice.current.model
    // right nav bar item Button
    @IBOutlet weak var navBtnItem_layout: UIBarButtonItem!
    // UI iPad
    @IBOutlet weak var iPadScheduleTimeOffRequestCollectionView: UICollectionView!
    @IBOutlet weak var iPadScheduleWeekDayDetailCollectionView: UICollectionView!
    
    @IBOutlet weak var iPadScheduleCalendar: FSCalendar!
    @IBOutlet weak var iPadScheduleWeekDayLabel: UILabel!
    // UI iPhone
    @IBOutlet weak var iPhoneScheduleWeekCollectionView: UICollectionView!
    
    // variable
    let WeekDayArray : [String] = ["Monday", "Tuesday", "Wednsday", "Thursday", "Friday", "Saturday", "Sunday"]
    // date
    var dateArray : [String] = []
    var daysToAdd = 0
    // firebase
    var company_UUID = ""
    var iPadSelectedDateScheduleEmployeeList = [ScheduleDayDetail?]()
    var indexPath_Row = 0
    var _employeesToDelete : [String] = []
    var _dateStringForDelete = ""
    
    var dayScheduleEmployeeList = [ScheduleDayDetail?]()
    var dayDetailEmployeeList = [[ScheduleDayDetail?]]()
    
    var noti_user_uuid_list : [String] = []
    
    var scheduleKeys : [String] = []
    
    var _isScheduleOnDelete = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // launch differnt mod based on device
        switch model {
        case "iPhone":
            navBtnItem_layout.title = "Time-Off Request"
            // collection views dataSource and delegate
            iPhoneScheduleWeekCollectionView.dataSource = self
            iPhoneScheduleWeekCollectionView.delegate = self
            // register cell for iPhone
            //            iPhoneScheduleWeekCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "scheduleCellPhone")
            //            iPhoneScheduleWeekCollectionView.register(UINib(nibName: "scheduleWeekiPhoneCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "scheduleCellPhone")
            iPhoneScheduleWeekCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "scheduleCellPhone")
            iPhoneScheduleWeekCollectionView.register(UINib(nibName: "scheduleWeekDayCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "scheduleCellPhone")
            break
        case "iPad":
            navBtnItem_layout.title = "Options"
            // collection views dataSource and delegate
            iPadScheduleTimeOffRequestCollectionView.dataSource = self
            iPadScheduleTimeOffRequestCollectionView.delegate = self
            iPadScheduleWeekDayDetailCollectionView.dataSource = self
            iPadScheduleWeekDayDetailCollectionView.delegate = self
            // register cell for iPad
            iPadScheduleWeekDayDetailCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "scheduleiPadWeekDetailCell")
            iPadScheduleWeekDayDetailCollectionView.register(UINib(nibName: "scheduleWeekDayCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "scheduleiPadWeekDetailCell")
            
            //            cellIdentifier = "scheduleiPadTimeOffRequestCell"
            
            // set default on calendar
            iPadScheduleCalendar.select(Date(), scrollToDate: true)
            let df = DateFormatter()
            iPadScheduleWeekDayLabel.text = "\((df.weekdaySymbols[Calendar.current.component(.weekday, from: Date())-1]).prefix(3).description)."
            break
        default:
            return
        }
        
        let defaults = UserDefaults.standard
        company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (model == "iPhone") {
            let startOfWeek = Date().startOfWeek
            dateArray.removeAll()
            dayScheduleEmployeeList.removeAll()
            dayDetailEmployeeList.removeAll()
            scheduleKeys.removeAll()
            
            for i in 1...7 {
                daysToAdd = i
                let _day = (Calendar.current as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: daysToAdd, to: startOfWeek, options: [])
                // get year, month, day, weekday here
                let _date = _day!.description.prefix(10).components(separatedBy: "-")
                let year = _date[0]
                let month = _date[1]
                let day = _date[2]
                // month-day-year for the firebase fetch
                let dateString = "\(month)-\(day)-\(year)"
                
                dateArray.append(dateString)
            }
            
            let defaults = UserDefaults.standard
            company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
//            var counter = 0
//            var counter2 = 0
            
            FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("schedule").observe(.value, with: { (snap) in
                if let dict = snap.value as? [String : AnyObject] {
                    for d in self.dateArray {
                        if (dict[d] != nil) {
                            // print("got some -\(d)-")
                            if let detail = dict[d] as? [String : AnyObject] {
                                self.dayScheduleEmployeeList.removeAll()
                                for keys in detail.keys {
                                    // print("-\(detail[keys]!)-")
                                    if let person = detail[keys] as? [String : AnyObject] {
                                        // print("-\(person)-")
                                        let schedulePerson = ScheduleDayDetail()
                                        
                                        schedulePerson.image = person["image"] as? String
                                        schedulePerson.name = person["name"] as? String
                                        schedulePerson.startTime = person["shift-start"] as? String
                                        schedulePerson.endTime = person["shift-end"] as? String
                                        
                                        self.dayScheduleEmployeeList.append(schedulePerson)
                                    }
                                }
                                // print("daySche cont \(self.dayScheduleEmployeeList.count)")
                                self.dayDetailEmployeeList.append(self.dayScheduleEmployeeList)
                            }
                        } else {
                            // print("#############nothing#############")
                            self.dayDetailEmployeeList.append([])
                        }
                        DispatchQueue.main.async(execute: {
                            self.iPhoneScheduleWeekCollectionView.reloadData()
                        })
                    }
                }
            })

        }//_end if iPhone
    }//_end viewWillAppear
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func navBtnItem_action(_ sender: Any) {
        switch model {
        case "iPhone":
            self.performSegue(withIdentifier: "scheduleToTimeOffRequestSegue", sender: self)
            break
        case "iPad":
            if (_isScheduleOnDelete) {
                // on edit
                _isScheduleOnDelete = false
                navBtnItem_layout.title = "Options"
                self.iPadScheduleWeekDayDetailCollectionView.reloadData()
            } else {
            // edit prompt action sheet
            let editPrompt = UIAlertController(title: "Options", message: "What are you trying to do?", preferredStyle: .actionSheet)
            // cancel selection
            editPrompt.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                // cancel and do nothing
            }))
            editPrompt.addAction(UIAlertAction(title: "Add Employee", style: .default, handler: { (action) in
                // new employee
                self.performSegue(withIdentifier: "scheduleiPadEditToAddEmployeeSegue", sender: self)
            }))
            editPrompt.addAction(UIAlertAction(title: "Remove Employee", style: .default, handler: { (action) in
                // delete employee
                self._isScheduleOnDelete = true
                self.iPadScheduleWeekDayDetailCollectionView.reloadData()
                self.navBtnItem_layout.title = "Done"
            }))
            editPrompt.addAction(UIAlertAction(title: "Time-Off Request", style: .default, handler: { (action) in
                // time off request
                self.performSegue(withIdentifier: "scheduleToTimeOffRequestSegue", sender: self)
            }))
            
            editPrompt.popoverPresentationController?.sourceView = self.view
            editPrompt.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX*1.9, y: self.view.bounds.midY/2, width: 0, height: 0)
            
            present(editPrompt, animated: true, completion: nil)
            }
            break
        default:
            return
        }
        
    }
    
}
