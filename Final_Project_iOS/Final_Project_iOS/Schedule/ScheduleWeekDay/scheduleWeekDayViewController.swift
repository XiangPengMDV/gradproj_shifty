

import UIKit
import Firebase

class scheduleWeekDayViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewTitle: UINavigationItem!
    
    var titleText = "Tuesday"
    var date = ""
    var dayDetailEmployeeList = [ScheduleDayDetail?]()
    var company_UUID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        viewTitle.title = titleText
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "scheduleDayDetailiPhoneCell")
        collectionView.register(UINib(nibName: "scheduleWeekDayCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "scheduleDayDetailiPhoneCell")
        
        let defaults = UserDefaults.standard
        company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("schedule").child(date).observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                let schedulePerson = ScheduleDayDetail()
                
                schedulePerson.image = dictionary["image"] as? String
                schedulePerson.name = dictionary["name"] as? String
                schedulePerson.startTime = dictionary["shift-start"] as? String
                schedulePerson.endTime = dictionary["shift-end"] as? String
                
                self.dayDetailEmployeeList.append(schedulePerson)
                
                print(self.dayDetailEmployeeList.count)
                
                DispatchQueue.main.async(execute: {
                    self.collectionView.reloadData()
                })
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dayDetailEmployeeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scheduleDayDetailiPhoneCell", for: indexPath) as! scheduleWeekDayCollectionViewCell
        
        cell.name.text = dayDetailEmployeeList[indexPath.row]?.name
        let timeString = "\((dayDetailEmployeeList[indexPath.row]?.startTime)!) - \((dayDetailEmployeeList[indexPath.row]?.endTime)!)"
        cell.time.text = timeString
        
        if let URL = dayDetailEmployeeList[indexPath.row]?.image {
            cell.image.loadImageUsingCache(URL)
        }
        
        return cell
    }
    
    // MARK: Size & Layout ================================================================================
    // size can all remain the same since the "collectionView" depends on each one of them
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // spacing in between each individual cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    // size for each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height/6)
    }
    // margin spacing for 4-sides
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
    }
    // =========================================================================
}
