

import Foundation
import Firebase

extension scheduleViewController : FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // get year, month, day, weekday here
        let _date = date.description.prefix(10).components(separatedBy: "-")
        let year = _date[0]
        let month = _date[1]
        let day = _date[2]
        // month-day-year for the firebase fetch
        let dateString = "\(month)-\(day)-\(year)"
        
        let df = DateFormatter()
        let weekday = "\((df.weekdaySymbols[Calendar.current.component(.weekday, from: date)-1]).prefix(3).description)."
        
        iPadScheduleWeekDayLabel.text = weekday
        self.iPadSelectedDateScheduleEmployeeList.removeAll()
        // get selected date schedule employees list
        self._dateStringForDelete = dateString
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("schedule").child(dateString).observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                let schedulePerson = ScheduleDayDetail()

                schedulePerson.image = dictionary["image"] as? String
                schedulePerson.name = dictionary["name"] as? String
                schedulePerson.startTime = dictionary["shift-start"] as? String
                schedulePerson.endTime = dictionary["shift-end"] as? String
                
                self.iPadSelectedDateScheduleEmployeeList.append(schedulePerson)
                self._employeesToDelete.append(snapshot.key)
                
                // print(self.iPadSelectedDateScheduleEmployeeList.count)

                let tempID = dictionary["uuid"] as? String
                print("pending to noti uuid list \(tempID!)")
                self.noti_user_uuid_list.append(tempID!)
                
                DispatchQueue.main.async(execute: {
                    self.iPadScheduleWeekDayDetailCollectionView.reloadData()
                })
                
            }
            
        }, withCancel: nil)
        
        DispatchQueue.main.async(execute: {
            self.iPadScheduleWeekDayDetailCollectionView.reloadData()
        })
    }// _end calendar didSelect at
    
}// _end main scope

