
import UIKit
import Firebase

class dayOffViewController: UIViewController {
    
    @IBOutlet var lblDays: [UIButton]!
    
    let normalColor = UIColor(red: 197/255, green: 255/255, blue: 240/255, alpha: 1.0)
    let selectColor = UIColor(red: 98/255, green: 230/255, blue: 255/255, alpha: 1.0)
    
    var selectedDays : [String] = []
    var selectDayExist = false
    var daysOffBool : [Bool] = []
    
    var company_UUID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid).observe(.value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {

                // nil check
                if (dictionary["Monday"] != nil) {
                    self.daysOffBool.append(dictionary["Monday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                if (dictionary["Tuesday"] != nil) {
                    self.daysOffBool.append(dictionary["Tuesday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                if (dictionary["Wednesday"] != nil) {
                    self.daysOffBool.append(dictionary["Wednesday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                if (dictionary["Thursday"] != nil) {
                    self.daysOffBool.append(dictionary["Thursday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                if (dictionary["Friday"] != nil) {
                    self.daysOffBool.append(dictionary["Friday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                if (dictionary["Saturday"] != nil) {
                    self.daysOffBool.append(dictionary["Saturday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                if (dictionary["Sunday"] != nil) {
                    self.daysOffBool.append(dictionary["Sunday"] as! Bool)
                } else {
                    self.daysOffBool.append(false)
                }
                
                for day in self.lblDays {
                    if (self.daysOffBool[day.tag - 1]) {
                        DispatchQueue.main.async(execute: {
                            day.backgroundColor = self.selectColor
                            day.setBackgroundImage(UIImage(named: "CancelIcon"), for: .normal)
                        })
                        self.appendDay(day: day.tag)
                    } else {
                        DispatchQueue.main.async(execute: {
                            day.backgroundColor = self.normalColor
                            day.setBackgroundImage(nil, for: .normal)
                        })
                    }
                }
            }
        }, withCancel: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionUpdateDaysOff(_ sender: Any) {
//        print(" -- content, \(selectedDays) -- ")
//        print(daysOffBool)
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let ref = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid)

        if (daysOffBool.count > 0) {
            if (daysOffBool[0]) {
                ref.child("Monday").setValue(true)
            } else {
                ref.child("Monday").setValue(false)
            }
            if (daysOffBool[1]) {
                ref.child("Tuesday").setValue(true)
            } else {
                ref.child("Tuesday").setValue(false)
            }
            if (daysOffBool[2]) {
                ref.child("Wednesday").setValue(true)
            } else {
                ref.child("Wednesday").setValue(false)
            }
            if (daysOffBool[3]) {
                ref.child("Thursday").setValue(true)
            } else {
                ref.child("Thursday").setValue(false)
            }
            if (daysOffBool[4]) {
                ref.child("Friday").setValue(true)
            } else {
                ref.child("Friday").setValue(false)
            }
            if (daysOffBool[5]) {
                ref.child("Saturday").setValue(true)
            } else {
                ref.child("Saturday").setValue(false)
            }
            if (daysOffBool[6]) {
                ref.child("Sunday").setValue(true)
            } else {
                ref.child("Sunday").setValue(false)
            }
        }
    }
    
    @IBAction func actionSelectDays(_ sender: UIButton) {
        switch sender.tag {
        case 7:
            // Sunday
            checkSelectedDays(day: "Sunday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[6]) {
                daysOffBool[6] = false
            } else {
                daysOffBool[6] = true
            }
            // print("7 Sun \(daysOffBool)")
            break
        case 1:
            // Monday
            checkSelectedDays(day: "Monday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[0]) {
                daysOffBool[0] = false
            } else {
                daysOffBool[0] = true
            }
            // print("1 M \(daysOffBool)")
            break
        case 2:
            // Tuesday
            checkSelectedDays(day: "Tuesday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[1]) {
                daysOffBool[1] = false
            } else {
                daysOffBool[1] = true
            }
            // print("2 T \(daysOffBool)")
            break
        case 3:
            // Wednesday
            checkSelectedDays(day: "Wednesday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[2]) {
                daysOffBool[2] = false
            } else {
                daysOffBool[2] = true
            }
            // print("3 W \(daysOffBool)")
            break
        case 4:
            // Thurday
            checkSelectedDays(day: "Thursday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[3]) {
                daysOffBool[3] = false
            } else {
                daysOffBool[3] = true
            }
            // print("4 TH \(daysOffBool)")
            break
        case 5:
            // Friday
            checkSelectedDays(day: "Friday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[4]) {
                daysOffBool[4] = false
            } else {
                daysOffBool[4] = true
            }
            // print("5 F \(daysOffBool)")
            break
        case 6:
            // Saturday
            checkSelectedDays(day: "Saturday", button: lblDays[lblDays.index(of: sender)!])
            if (daysOffBool[5]) {
                daysOffBool[5] = false
            } else {
                daysOffBool[5] = true
            }
            // print("6 Sat \(daysOffBool)")
            break
        default:
            break
        }
    }//_end actionSelectDays()
    
    func checkSelectedDays (day : String, button : UIButton) {
        if (selectedDays.count > 0) {
            for d in selectedDays {
                if (d == day) {
                    selectDayExist = true
                    break
                } else {
                    selectDayExist = false
                }
            }
            print("loop end \(selectDayExist)")
            if (!selectDayExist) {
                selectedDays.append(day)
                button.backgroundColor = selectColor
                button.setBackgroundImage(UIImage(named: "CancelIcon"), for: .normal)
            } else {
                if let index = selectedDays.index(of: day) {
                    selectedDays.remove(at: index)
                    button.backgroundColor = normalColor
                    button.setBackgroundImage(nil, for: .normal)
                }
            }
        } else {
            selectedDays.append(day)
            button.backgroundColor = selectColor
        }
    }//_end checkSelectedDays()
    
    func appendDay (day : Int) {
        switch day {
        case 7:
            // Sunday
            self.selectedDays.append("Sunday")
            break
        case 1:
            // Monday
            self.selectedDays.append("Monday")
            break
        case 2:
            // Tuesday
            self.selectedDays.append("Tuesday")
            break
        case 3:
            // Wednesday
            self.selectedDays.append("Wednesday")
            break
        case 4:
            // Thurday
            self.selectedDays.append("Thursday")
            break
        case 5:
            // Friday
            self.selectedDays.append("Friday")
            break
        case 6:
            // Saturday
            self.selectedDays.append("Saturday")
            break
        default:
            break
        }
    }//_end appendDay()
}
