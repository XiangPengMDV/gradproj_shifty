
import UIKit
import Firebase

class profileEditViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //iPhone or iPad
    let model = UIDevice.current.model
    // UI on both
    @IBOutlet weak var profileEditImage: UIImageView!
    @IBOutlet weak var profileEditInputNewPhone: UITextField!
    @IBOutlet weak var profileEditInputNewEmail: UITextField!
    // UI on iPad
    @IBOutlet weak var profileEditWorkID: UILabel!
    @IBOutlet weak var profileEditName: UILabel!
    @IBOutlet weak var profileEditOccupation: UILabel!
    @IBOutlet weak var profileEditPhone: UILabel!
    @IBOutlet weak var profileEditEmail: UILabel!
    @IBOutlet var containerView: UIView!
    
    var company_UUID : String = ""
    // email for user to login
    var email = ""
    // show local image for _user_image_change
    var imagePicker = UIImagePickerController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imagePicker.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let defaults = UserDefaults.standard
        self.company_UUID = defaults.string(forKey: UserDefaultsKeys.key_companyUUID)!
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let userRef = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid)
        userRef.observe(.value, with: { (snapShot) in
            // data
            if let dictionary = snapShot.value as? [String : AnyObject] {
                // fill out the UI show data
                self.profileEditName.text = dictionary["name"] as? String
                self.profileEditOccupation.text = dictionary["occupation"] as? String
                self.profileEditEmail.text = dictionary["email"] as? String
                self.profileEditPhone.text = dictionary["phone-number"] as? String
                self.profileEditWorkID.text = dictionary["workID"] as? String
                
                
                self.email = (FIRAuth.auth()?.currentUser?.email)!
                self.profileEditInputNewEmail.text = self.profileEditEmail.text
                self.profileEditInputNewPhone.text = self.profileEditPhone.text
                
                //Photo URL
                let userPhoto = dictionary["image"] as? String
                //image exists
                if let url = userPhoto {
                    let caughtURL = URL(string: url)
                    URLSession.shared.dataTask(with: caughtURL!, completionHandler: { (data, response, error) in
                        if error != nil {
                            print(error!)
                            return
                        }
                        DispatchQueue.main.async(execute: {
                            self.profileEditImage.image = UIImage(data: data!)
                        })
                    }).resume()
                }
            }
        }, withCancel: nil)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        containerView.addGestureRecognizer(tapGesture)
    }// _end  viewDidLoad()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissKeyboard (_ sender : AnyObject) {
        view.endEditing(true)
    }
    
    @IBAction func changeProfilePicture_action(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func changePassword_action(_ sender: Any) {
        FIRAuth.auth()?.sendPasswordReset(withEmail: email, completion: { (error) in
            var title = ""
            var message = ""
            // if error or not, tell the user whats going no
            if error != nil {
                title = "Error!"
                message = (error?.localizedDescription)!
            } else {
                title = "Success!"
                message = "Password reset email sent."
            }
            // show the result
            let resultAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let resultOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            resultAlert.addAction(resultOK)
            
            self.present(resultAlert, animated: true, completion: nil)

        })
    }// _end changePassword_action(_ sender: Any)
    
    @IBAction func save_action(_ sender: Any) {
        let newEmail = profileEditInputNewEmail.text
        let newPhone = profileEditInputNewPhone.text
        if ((newEmail != nil && newEmail != "" && (newEmail?.characters.count)! > 0) || (newPhone != nil && newPhone != "" && (newPhone?.characters.count)! > 0)) {
            // not nil, not empty, at least 1 chara
            if (!(newEmail?.contains("@"))! || !(newEmail?.contains("."))!){
                let resultAlert = UIAlertController(title: "Invalid Email", message: "Pleaes double check your email input before save", preferredStyle: .alert)
                let resultOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                resultAlert.addAction(resultOK)
                self.present(resultAlert, animated: true, completion: nil)
            } else {
                guard let uid = FIRAuth.auth()?.currentUser?.uid else {
                    return
                }
                let ref = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid)
                ref.child("email").setValue(newEmail!)
                ref.child("phone-number").setValue(newPhone!)
                
                let resultAlert = UIAlertController(title: "Succeed", message: "New Email / Phone Number Saved", preferredStyle: .alert)
                let resultOK = UIAlertAction(title: "OK", style: .default, handler: nil)
                resultAlert.addAction(resultOK)
                self.present(resultAlert, animated: true, completion: nil)
                if (model == "iPad") {
                    profileEditEmail.text = newEmail!
                    profileEditPhone.text = newPhone!
                }
            }
        } else {
            let resultAlert = UIAlertController(title: "Empty Fields", message: "Attempt to change email or phone number with empty input field", preferredStyle: .alert)
            let resultOK = UIAlertAction(title: "OK", style: .default, handler: nil)
            resultAlert.addAction(resultOK)
            self.present(resultAlert, animated: true, completion: nil)
        }

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info["UIImagePickerControllerOriginalImage"] as! UIImage
        self.profileEditImage.image = image
        if let imageData = UIImagePNGRepresentation(self.profileEditImage.image!) {
            guard let uid = FIRAuth.auth()?.currentUser?.uid else {
                return
            }
            FIRStorage.storage().reference().child("profile_images").child(uid).put(imageData, metadata: nil, completion: { (metadata, error) in
                if (error != nil){
                    print(error!)
                    return
                }
                if let downloadURL = metadata?.downloadURL()?.absoluteString {
             FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid).child("image").setValue(downloadURL)
                }
            })
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}// _end Main Scope
