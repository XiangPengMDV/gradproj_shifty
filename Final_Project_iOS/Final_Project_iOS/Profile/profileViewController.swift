import UIKit
import Firebase

class profileViewController: UIViewController {
    //iPhone or iPad
    let model = UIDevice.current.model
    // profile UI
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileWorkID: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileOccupation: UILabel!
    @IBOutlet weak var profilePhone: UILabel!
    @IBOutlet weak var proileEmail: UILabel!
    
    @IBOutlet weak var profileNavBtnEditOutlet: UIBarButtonItem!
    
    // collectionView
    @IBOutlet weak var profileRequestCollectionView: UICollectionView!
    
    // variables
    var company_UUID : String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        //Checking is user is a valid user based off of the existence of their name
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let userRef = FIRDatabase.database().reference().child("Users").child(uid)
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if (snapshot.value as? [String: AnyObject]) != nil {
                // signed in
            } else {
                DispatchQueue.main.async(execute: {
                    do {
                        try FIRAuth.auth()?.signOut()
                        // this dismiss the whole tab bar view controller
                        self.dismiss(animated: true, completion: nil)
                    } catch let signOutError as NSError {
                        print ("Error signing out: \(signOutError)")
                    }
                })
            }
        }, withCancel: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // present diffrent UI based on devices
        switch model {
        case "iPhone":
            profileNavBtnEditOutlet.title = "Edit"
            profileNavBtnEditOutlet.isEnabled = true
            break
        case "iPad":
//            profileRequestCollectionView.dataSource = self
//            profileRequestCollectionView.delegate = self
            profileNavBtnEditOutlet.title = ""
            profileNavBtnEditOutlet.isEnabled = false
            break
        default:
            break
        }
        // show profile if user logged in, else back to login screen
        if (FIRAuth.auth()?.currentUser?.uid == nil) {
            dismiss(animated: true, completion: nil)
        } else {
            // USER LOGGED IN
            guard let uid = FIRAuth.auth()?.currentUser?.uid else {
                return
            }
            // grab company uuid from users
            let idRef = FIRDatabase.database().reference().child("Users").child(uid)

            idRef.observe(.value, with: { (snapShot) in
                if let dic = snapShot.value as? [String : AnyObject] {
                    self.company_UUID = dic["company-uuid"] as! String
                    // save it in local
                    let defaults = UserDefaults.standard
                    defaults.set(self.company_UUID, forKey: UserDefaultsKeys.key_companyUUID)
                    // get data
                    let userRef = FIRDatabase.database().reference().child("Companies").child(self.company_UUID).child("employees").child(uid)
                    userRef.observe(.value, with: { (snapShot) in
                        // data
                        if let dictionary = snapShot.value as? [String : AnyObject] {
                            // fill out the UI show data
                            self.profileName.text = dictionary["name"] as? String
                            self.profileOccupation.text = dictionary["occupation"] as? String
                            self.proileEmail.text = dictionary["email"] as? String
                            self.profilePhone.text = dictionary["phone-number"] as? String
                            self.profileWorkID.text = dictionary["workID"] as? String
                            //Photo URL
                            let userPhoto = dictionary["image"] as? String
                            //image exists
                            if let url = userPhoto {
                                let caughtURL = URL(string: url)
                                URLSession.shared.dataTask(with: caughtURL!, completionHandler: { (data, response, error) in
                                    if error != nil {
                                        print(error!)
                                        return
                                    }
                                    DispatchQueue.main.async(execute: {
                                        self.profileImage.image = UIImage(data: data!)
                                    })
                                }).resume()
                            }
                        }
                    }, withCancel: nil)
                }
            })

        }// _end if (FIRAuth.auth()?.currentUser?.uid == nil){}
        
    }// _end viewDidLoad()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func logOut_action(_ sender: Any) {
        // authorizing the singout
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            // this dismiss the whole tab bar view controller
            self.parent?.dismiss(animated: true, completion: nil)
        } catch let signOutError as NSError {
            print ("Error signing out: \(signOutError)")
        }
    }
    
    @IBAction func edit_action(_ sender: Any) {
        performSegue(withIdentifier: "profileToEditProfileSegue", sender: self)
    }
}


