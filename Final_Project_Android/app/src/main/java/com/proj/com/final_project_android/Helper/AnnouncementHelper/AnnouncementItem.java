package com.proj.com.final_project_android.Helper.AnnouncementHelper;

public class AnnouncementItem {
    String title;
    String datePosted;
    String uuid;

    public AnnouncementItem(String title, String datePosted, String uuid) {
        this.title = title;
        this.datePosted = datePosted;
        this.uuid = uuid;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public String getTitle() {
        return title;
    }

    public String getUuid() {
        return uuid;
    }
}
