package com.proj.com.final_project_android.Helper.ScheduleHelper;

import android.support.annotation.Nullable;


public class AddScheduleEmployeeItem {
    String name;
    String image;
    String uuid;
    String access;
    Boolean sunday;
    Boolean monday;
    Boolean tuesday;
    Boolean wednesday;
    Boolean thursday;
    Boolean friday;
    Boolean saturday;

    public AddScheduleEmployeeItem(String name, String image, String uuid, String access, @Nullable Boolean sunday,
                                   @Nullable Boolean monday, @Nullable Boolean tuesday, @Nullable Boolean wednesday,
                                   @Nullable Boolean thursday, @Nullable Boolean friday, @Nullable Boolean saturday) {
        this.name = name;
        this.image = image;
        this.uuid = uuid;
        this.access = access;
        this.sunday = sunday;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUuid() {
        return uuid;
    }

    public String getAccess() {
        return access;
    }

    public Boolean getSunday() {
        return sunday;
    }

    public Boolean getMonday() {
        return monday;
    }

    public Boolean getTuesday() {
        return tuesday;
    }

    public Boolean getWednesday() {
        return wednesday;
    }

    public Boolean getThursday() {
        return thursday;
    }

    public Boolean getFriday() {
        return friday;
    }

    public Boolean getSaturday() {
        return saturday;
    }
}
