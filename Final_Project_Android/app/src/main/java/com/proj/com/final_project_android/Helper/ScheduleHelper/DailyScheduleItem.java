package com.proj.com.final_project_android.Helper.ScheduleHelper;


import android.support.annotation.Nullable;

public class DailyScheduleItem {
    String image;
    String name;
    String time;
    String uuid;

    public DailyScheduleItem(@Nullable String image, String name, String time, String uuid) {
        this.image = image;
        this.name = name;
        this.uuid = uuid;
        this.time = time;
    }

    public String getUuid() {
        return uuid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
