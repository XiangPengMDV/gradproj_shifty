package com.proj.com.final_project_android.Fragment.Profile;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;


public class ProfileFragment extends Fragment implements View.OnClickListener {
    //universal
    ImageView profileImage;
    TextView workId;
    TextView name;
    TextView accessLevel;
    TextView phoneNumber;
    TextView emailT;
    String mCompanyId;
    private StorageReference mStorageRef;
    //tablet
    Button editProfile;
    Button changePassword;
    //Firebase
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    //Overrides
    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    //enabling access to menu items
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }
    //Re pulling data onResume to recheck for network
    @Override
    public void onResume() {
        super.onResume();
        pullFromFirebase();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(getResources().getBoolean(R.bool.isTablet)){
            menu.findItem(R.id.edit_profile_menu).setVisible(false);
        } else {
            menu.findItem(R.id.edit_profile_menu).setVisible(true);
        }
        menu.findItem(R.id.new_message_menu).setVisible(false);
        //menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(false);
        menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.edit_profile_menu){
        showEditProfile();
        }
        return super.onOptionsItemSelected(item);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        //Universal initialization
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Profile");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Profile");
        }

        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        boolean isTablet = getActivity().getResources().getBoolean(R.bool.isTablet);
        profileImage = (ImageView) getActivity().findViewById(R.id.profile_image);
        workId = (TextView) getActivity().findViewById(R.id.edit_work_id_text);
        name = (TextView) getActivity().findViewById(R.id.edit_name_text);
        accessLevel = (TextView) getActivity().findViewById(R.id.edit_access_level_text);
        phoneNumber = (TextView) getActivity().findViewById(R.id.phone_number_text);
        emailT = (TextView) getActivity().findViewById(R.id.email_text);
        if(isTablet){
            loadTabletUI();
        }
        pullFromFirebase();
        checkForNotifications();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_profile_button:
                //Load detail edit profile
                showTabletEditProfileFragment();
                break;
            case R.id.change_password_button:
                //Load detail change password
                showChangePasswordDialog();
                break;
        }
    }

    //My Functions
    private void showChangePasswordDialog(){
        AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater customInflater = getActivity().getLayoutInflater();
        final View dialogView = customInflater.inflate(R.layout.edit_profile_change_password_dialog, null);
        newPasswordDialogBuilder.setView(dialogView);
        final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
        newPasswordDialogBuilder.setTitle("Change Password");
        newPasswordDialogBuilder.setMessage("Please Enter your old Password then your new one.(At least 6 Characters)");
        newPasswordDialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //change password in firebase
                if(newPasswordET.getText().toString().isEmpty()){
                    //one of the fields is empty
                    Toast.makeText(getContext(), "No Password Entered, Please try again.", Toast.LENGTH_LONG).show();
                } else if (newPasswordET.getText().toString().length() < 6) {
                    //password too short
                    Toast.makeText(getContext(), "New Password was not atleast 6 characters, Please try again.", Toast.LENGTH_LONG).show();
                } else {
                            //all good change password
                            FirebaseAuth.getInstance().getCurrentUser().updatePassword(newPasswordET.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            SharedPreferences pref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(getString(R.string.user_password_key), newPasswordET.getText().toString());
                            editor.apply();
                            Toast.makeText(getContext(), "Password Updated!", Toast.LENGTH_SHORT).show();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //failed
                            Toast.makeText(getContext(), "Failed to change password in firebase", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });
        newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //do nothing
            }
        });
        AlertDialog mainAlert = newPasswordDialogBuilder.create();
        mainAlert.show();
    }
    //Adds to backstack to allow back n forth betwen profile and edit profile
    public void showEditProfile(){
        //phone will load edit profile screen
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                EditProfileFragment.newInstance(),
                EditProfileFragment.TAG_Main
        ).addToBackStack(null).commit();
    }
    //Tablet Functions
    public void loadTabletUI(){
        editProfile = (Button) getActivity().findViewById(R.id.edit_profile_button);
        changePassword = (Button) getActivity().findViewById(R.id.change_password_button);
        if(editProfile != null && changePassword != null){
            editProfile.setOnClickListener(this);
            changePassword.setOnClickListener(this);
        }
    }
    private void showTabletEditProfileFragment(){
        //Replaces detail with edit profile fragment
        getFragmentManager().beginTransaction().replace(
                R.id.profile_container_right,
                EditProfileFragmentTablet.newInstance(),
                EditProfileFragmentTablet.TAG_Main
        ).addToBackStack(null).commit();
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Edit Profile");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Edit Profile");
        }
        //take this off if you want to go back a fragment
        //getActivity().getFragmentManager().popBackStack();
    }
    //Firebase Funcs
    public void pullFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //Network connection is a go
        if(isConnected){
            final long ONE_MEGABYTE = 1024 * 1024;
            final StorageReference imageRef = mStorageRef.child("profile_images/" + currentUser.getUid());
            //mCompanyId = dataSnapshot.child("company-uuid").getValue(String.class);
            final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
            companyRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //name
                    if(dataSnapshot.child("name").getValue(String.class) != null){
                        name.setText(dataSnapshot.child("name").getValue(String.class));
                    }
                    //access level
                    if(dataSnapshot.child("occupation").getValue(String.class) != null){
                        accessLevel.setText(dataSnapshot.child("occupation").getValue(String.class));
                    }
                    //email
                    if(dataSnapshot.child("email").getValue(String.class) != null){
                        emailT.setText(dataSnapshot.child("email").getValue(String.class));
                    }
                    //phone
                    if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                        //phone exists
                        phoneNumber.setText(dataSnapshot.child("phone-number").getValue(String.class));
                    }
                    //work-id
                    if(dataSnapshot.child("workID").getValue(String.class) != null){
                        workId.setText(dataSnapshot.child("workID").getValue(String.class));
                    }
                    //image
                    imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                @Override
                                public void onSuccess(byte[] bytes) {
                                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                    profileImage.setImageBitmap(bmp);
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            //no update
                        }
                    });
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }




    }

    public void checkForNotifications(){

        final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees")
                .child(currentUser.getUid()).child("notifications");


        companyRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.child("notified").getValue(Boolean.class) != null){
                    if(!dataSnapshot.child("notified").getValue(Boolean.class)){
                        int i = 1;
                        //false
                        NotificationCompat.Builder mBuilder =
                                new NotificationCompat.Builder(getContext())
                                        .setSmallIcon(R.drawable.shifty_logo)
                                        .setContentTitle("Schedule Update")
                                        .setContentText(dataSnapshot.child("notificationText").getValue(String.class));
                        // Gets an instance of the NotificationManager service
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.notify(i, mBuilder.build());

                        database.getReference().child("Companies").child(mCompanyId)
                                .child("employees").child(currentUser.getUid()).child("notifications").child(dataSnapshot.getKey())
                                .child("notified").setValue(true);
                        i = i+1;
                    } else {
                        //true
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



}
