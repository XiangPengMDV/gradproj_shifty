package com.proj.com.final_project_android.Fragment.Employees;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;


public class AddEmployee extends Fragment {


    final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    EditText email;
    EditText name;
    EditText password;
    Spinner accessLevel;

    //overrides
    public static AddEmployee newInstance() {
        Bundle args = new Bundle();
        AddEmployee fragment = new AddEmployee();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.save_new_employee_menu).setVisible(true);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(true);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_new_employee_menu:
                createNewEmployee();
                break;
            case R.id.cancel_new_employee_menu:
                cancelNewEmployee();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_employee_fragment, container, false);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Add Employee");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Add Employee");
        }
        email = (EditText) getActivity().findViewById(R.id.add_employee_email);
        name = (EditText) getActivity().findViewById(R.id.add_employee_name);
        password = (EditText) getActivity().findViewById(R.id.add_employee_password);
        accessLevel = (Spinner) getActivity().findViewById(R.id.add_employee_access_level_spinner);
        super.onActivityCreated(savedInstanceState);
    }

    //MY Funcs
    public void cancelNewEmployee(){
        if(getResources().getBoolean(R.bool.isTablet)){
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    EmployeesFragmentTablet.newInstance()
            ).commit();
        } else {
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    EmployeesFragment.newInstance()
            ).commit();
        }

    }
    public Boolean addEmployeeInputValidation(){
        if(email.getText().toString().isEmpty() || name.getText().toString().isEmpty() || password.getText().toString().isEmpty()){
            //something is empty
            Toast.makeText(getContext(), "Please fill in all the fields.", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if(Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
                //proper email
                if(password.getText().length() < 6){
                    //password to short
                    Toast.makeText(getContext(), "Password must be atleast 6 characters long", Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    //Everything is good here
                    return true;
                }
            } else {
                //improper email
                Toast.makeText(getContext(), "Improper Email", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    //Firebase
    public void createNewEmployee(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            if(addEmployeeInputValidation()){
                //all inputs are good
                final String currentEmail = mAuth.getCurrentUser().getEmail();
                mAuth.createUserWithEmailAndPassword(email.getText().toString().trim(), password.getText().toString().trim()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Toast.makeText(getContext(), "Employee created", Toast.LENGTH_SHORT).show();
                        SharedPreferences pref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                        DatabaseReference ref = database.getReference().child("Users").child(mAuth.getCurrentUser().getUid());
                        ref.child("company-uuid").setValue(pref.getString(getString(R.string.user_company_key), "N/A"));
                        DatabaseReference companyRef = database.getReference().child("Companies").child(pref.getString(getString(R.string.user_company_key), "N/A")).child("employees").child(mAuth.getCurrentUser().getUid());
                        companyRef.child("name").setValue(name.getText().toString().trim());
                        companyRef.child("email").setValue(email.getText().toString().trim());
                        companyRef.child("occupation").setValue(accessLevel.getSelectedItem().toString());
                        mAuth.signInWithEmailAndPassword(currentEmail, pref.getString(getString(R.string.user_password_key), "N/A")).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                //go back to employee screen
                                if(getResources().getBoolean(R.bool.isTablet)){
                                    getFragmentManager().beginTransaction().replace(R.id.core_activity_main_content,
                                            EmployeesFragmentTablet.newInstance()).commit();
                                } else {
                                    getFragmentManager().beginTransaction().replace(R.id.core_activity_main_content,
                                            EmployeesFragment.newInstance()).commit();
                                }

                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Unable to create Employee", Toast.LENGTH_LONG).show();
                    }
                });
            }

        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            createNewEmployee();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }




    }


}
