package com.proj.com.final_project_android.Fragment.Employees;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.Helper.EmployeeHelper.EmployeeAdapterTablet;
import com.proj.com.final_project_android.Helper.EmployeeHelper.EmployeeItem;
import com.proj.com.final_project_android.R;

import java.util.ArrayList;


public class EmployeesFragmentTablet extends Fragment implements AdapterView.OnItemClickListener {

    ImageView detailImage;
    TextView detailWorkId;
    TextView detailName;
    TextView detailAccess;
    TextView detailPhone;
    TextView detailEmail;
    Boolean mEdit = false;
    Menu mMenu;
    ConstraintLayout detailContainer;
    private ListView mListView;
    private StorageReference mStorageRef;
    private EmployeeAdapterTablet mAdapter;
    private ArrayList<EmployeeItem> mList;
    private ArrayList<EmployeeItem> deleteArray;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    //Overrides
    public static EmployeesFragmentTablet newInstance() {
        Bundle args = new Bundle();
        EmployeesFragmentTablet fragment = new EmployeesFragmentTablet();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.new_message_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(true);
        menu.findItem(R.id.edit_employee_menu).setVisible(true);

        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        //menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        //menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(false);
        menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_employee_menu:
                showAddProfileTablet();
                break;
            case R.id.edit_employee_menu:
                editEmployees();
                break;
            case R.id.save_edit_employee_menu:
                deleteSelectedEmployees();
                break;
            case R.id.cancel_edit_employee_menu:
                cancelEditEmployees();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employee_fragment_tablet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Employees");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Employees");
        }
        deleteArray = new ArrayList<>();
        detailImage = (ImageView) getActivity().findViewById(R.id.employee_detail_image_tablet);
        detailWorkId = (TextView) getActivity().findViewById(R.id.employee_work_id_tablet);
        detailName = (TextView) getActivity().findViewById(R.id.employee_detail_name);
        detailAccess = (TextView) getActivity().findViewById(R.id.employee_detail_access);
        detailPhone = (TextView) getActivity().findViewById(R.id.employee_detail_phone);
        detailEmail = (TextView) getActivity().findViewById(R.id.employee_detail_email);
        detailContainer = (ConstraintLayout) getActivity().findViewById(R.id.detail_container);
        detailContainer.setVisibility(View.INVISIBLE);
        mListView = (ListView) getActivity().findViewById(R.id.employee_list_tablet);
        mListView.setSelector(R.color.colorPrimary);
        mList = new ArrayList<>();
        mAdapter = new EmployeeAdapterTablet(getContext(), mList, mEdit);
        mListView.setAdapter(mAdapter);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        mListView.setOnItemClickListener(this);
        pullEmployeesFromFirebase();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mEdit){
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.tablet_employee_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
            }
        }
            detailContainer.setVisibility(View.VISIBLE);
            detailWorkId.setText("Work ID");
            detailName.setText("Name");
            detailPhone.setText("Phone Number");
            detailEmail.setText("Email");
            detailAccess.setText("Access Level");
            detailImage.setImageDrawable(getActivity().getDrawable(R.drawable.shifty_logo));
            pullEmployeeDetailFromFirebase(mList.get(i).getUuid());
    }

    //My Functions
    public void showAddProfileTablet(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AddEmployee.newInstance()
        ).addToBackStack(null).commit();
    }
    public void cancelEditEmployees(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Employees");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Employees");
        }
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.add_employee_menu).setVisible(true);
    }
    public void editEmployees(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Edit Employees");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Edit Employees");
        }
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.add_employee_menu).setVisible(false);

    }




    //Firebase Func
    public void pullEmployeesFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network find
        if(isConnected){
            //company id was already found
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees");
            ref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mList.add(new EmployeeItem(dataSnapshot.child("name").getValue(String.class),
                            dataSnapshot.child("image").getValue(String.class), dataSnapshot.getKey(), dataSnapshot.child("occupation").getValue(String.class)));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else {
            //no network
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mList = new ArrayList<EmployeeItem>();
                            pullEmployeesFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }
    public void pullEmployeeDetailFromFirebase(final String UUID){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            final StorageReference imageRef = mStorageRef.child("profile_images/" + UUID);
            final long ONE_MEGABYTE = 1024 * 1024;
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees").child(UUID);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("name").getValue(String.class) != null){
                        detailName.setText(dataSnapshot.child("name").getValue(String.class));
                        if(getResources().getBoolean(R.bool.isTablet)){
                            ((TabletCoreActivity)getActivity()).setTheDamnTitle(dataSnapshot.child("name").getValue(String.class));
                        }
                    }
                    if(dataSnapshot.child("email").getValue(String.class) != null){
                        detailEmail.setText(dataSnapshot.child("email").getValue(String.class));
                    }
                    if(dataSnapshot.child("occupation").getValue(String.class) != null){
                        detailAccess.setText(dataSnapshot.child("occupation").getValue(String.class));
                    }
                    if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                        //phone exists
                        detailPhone.setText(dataSnapshot.child("phone-number").getValue(String.class));
                    }
                    if(dataSnapshot.child("workID").getValue(String.class) != null){
                        detailWorkId.setText(dataSnapshot.child("workID").getValue(String.class));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //nothing
                }
            });
            imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    detailImage.setImageBitmap(bmp);
                }
            });
        } else {
            // no network
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullEmployeeDetailFromFirebase(UUID);
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }




    }
    public void deleteSelectedEmployees(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            for(EmployeeItem employee: deleteArray){
                final DatabaseReference userToDelete = database.getReference()
                        .child("Companies").child(mCompanyId).child("employees").child(employee.getUuid());
                final DatabaseReference userBranch = database.getReference().child("Users").child(employee.getUuid());
                userBranch.removeValue();
                userToDelete.removeValue();
//                final DatabaseReference userToDelete = FirebaseDatabase.getInstance().getReference()
//                        .child("Companies").child(mCompanyId).child("employees").child(employee.getUuid());
//                userToDelete.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        mEdit = false;
//                        mAdapter.updateEdit(false);

//                    }
//                });
            }
            mMenu.findItem(R.id.save_edit_employee_menu).setVisible(false);
            mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
            mMenu.findItem(R.id.edit_employee_menu).setVisible(true);
            mMenu.findItem(R.id.add_employee_menu).setVisible(true);
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    EmployeesFragmentTablet.newInstance()
            ).commit();
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteSelectedEmployees();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }
}
