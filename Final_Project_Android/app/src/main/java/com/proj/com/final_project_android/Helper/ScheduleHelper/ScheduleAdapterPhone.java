package com.proj.com.final_project_android.Helper.ScheduleHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.proj.com.final_project_android.R;

import java.util.ArrayList;
import java.util.TreeSet;


public class ScheduleAdapterPhone extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<String> day_list;
    private final ArrayList<String> week_days;
    private TreeSet<Integer> sectionHeader = new TreeSet<>();



    public ScheduleAdapterPhone(Context context, ArrayList<String> day_list) {
        this.context = context;
        this.day_list = day_list;
        this.week_days = new ArrayList<>();
        week_days.add("Monday");
        week_days.add("Tuesday");
        week_days.add("Wednesday");
        week_days.add("Thursday");
        week_days.add("Friday");
        week_days.add("Saturday");
        week_days.add("Sunday");
    }




    @Override
    public int getCount() {
        return (week_days != null) ? week_days.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return week_days.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.schedule_phone_list_view_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.weekday.setText(week_days.get(i));
        holder.date.setText(day_list.get(i));
        return view;
    }

    static class ViewHolder {
        final TextView weekday;
        final TextView date;

        ViewHolder(View v) {
            weekday = (TextView) v.findViewById(R.id.schedule_weekday);
            date = (TextView) v.findViewById(R.id.schedule_date);
        }
    }
}
