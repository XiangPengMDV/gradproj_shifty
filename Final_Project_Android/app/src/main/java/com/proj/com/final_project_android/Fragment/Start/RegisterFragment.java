package com.proj.com.final_project_android.Fragment.Start;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.R;

import java.util.UUID;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    EditText nameET;
    EditText companyET;
    EditText emailET;
    EditText passwordET;
    EditText confirmPasswordET;
    Button completeSignUpB;


    public static RegisterFragment newInstance() {
        Bundle args = new Bundle();
        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        nameET = (EditText) getActivity().findViewById(R.id.name_input_register);
        companyET = (EditText) getActivity().findViewById(R.id.company_input_register);
        emailET = (EditText) getActivity().findViewById(R.id.email_input_register);
        passwordET = (EditText) getActivity().findViewById(R.id.password_input_register);
        confirmPasswordET = (EditText) getActivity().findViewById(R.id.confirmp_input_register);
        completeSignUpB = (Button) getActivity().findViewById(R.id.signup_button_register);
        completeSignUpB.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }
    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.signup_button_register) {
            if (registerInputValidation()) {
                signUpWithFirebase(emailET.getText().toString().trim(), passwordET.getText().toString().trim());
            }
        }
    }

    public Boolean registerInputValidation(){
        //Input checks will be here
        if(nameET.getText().toString().isEmpty() || companyET.getText().toString().isEmpty() || emailET.getText().toString().isEmpty()
                || passwordET.getText().toString().isEmpty() || confirmPasswordET.getText().toString().isEmpty()){
            //something is empty
            Toast.makeText(getContext(), "Please Fill In all Fields", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            //all fields filled in with something
            if(Patterns.EMAIL_ADDRESS.matcher(emailET.getText().toString()).matches()){
                //valid email address
                if(passwordET.getText().toString().equals(confirmPasswordET.getText().toString())) {
                    //password and confirm password match
                    return true;
                } else {
                    //passwords don't match
                    Toast.makeText(getContext(), "Passwords Do Not Match", Toast.LENGTH_LONG).show();
                    return false;
                }
            } else {
                //invalid email address
                Toast.makeText(getContext(), "Improper Email Format", Toast.LENGTH_LONG).show();
                return false;
            }
        }
    }

    private void signUpWithFirebase(String email, final String password){
        mAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(getActivity(), new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                //user created
                //generate company uuid
                String companyUUID = UUID.randomUUID().toString();
                DatabaseReference companyRef = mDatabase.getReference().child("Companies").child(companyUUID);
                companyRef.child("company-name").setValue(companyET.getText().toString());
                DatabaseReference employeeRef = companyRef.child("employees").child(authResult.getUser().getUid());
                employeeRef.child("email").setValue(authResult.getUser().getEmail());
                employeeRef.child("name").setValue(nameET.getText().toString());
                employeeRef.child("occupation").setValue("Boss");
                DatabaseReference userRef = mDatabase.getReference().child("Users").child(authResult.getUser().getUid()).child("company-uuid");
                userRef.setValue(companyUUID);
                //Send to coreActivity
                Intent coreIntent = new Intent();
                coreIntent.setClass(getActivity(), CoreActivity.class);
                getActivity().finish();
                startActivity(coreIntent);
            }
        }).addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "Failed To Register", Toast.LENGTH_LONG).show();
            }
        });
    }
}
