package com.proj.com.final_project_android.Helper.ChatHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.proj.com.final_project_android.Helper.EmployeeHelper.EmployeeItem;
import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class NewChatAdapter extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<EmployeeItem> new_chat_list;

    public NewChatAdapter(Context context, ArrayList<EmployeeItem> new_chat_list) {
        this.context = context;
        this.new_chat_list = new_chat_list;
    }

    @Override
    public int getCount() {
        return (new_chat_list != null) ? new_chat_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return new_chat_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.new_chat_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(new_chat_list.get(i).getName());
        Picasso.with(context)
                .load(new_chat_list.get(i).getImage())
                .into(holder.image);

        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.new_chat_image);
            name = (TextView) v.findViewById(R.id.new_chat_name);
        }
    }
}
