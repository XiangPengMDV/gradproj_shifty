package com.proj.com.final_project_android.Helper.ChatHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.proj.com.final_project_android.R;

import java.util.ArrayList;


public class CurrentChatAdapter extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final ArrayList<Object> listMain;
    private static final int SENDER = 0;
    private static final int RECIEVER = 1;
    private static final int NULL = 2;
    private LayoutInflater inflater;
    private Context context;

    public CurrentChatAdapter(Context context, ArrayList<Object> listMain) {
        this.listMain = listMain;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemViewType(int position) {
        if(listMain.get(position) instanceof CurrentChatItem){
            if(((CurrentChatItem) listMain.get(position)).getCurrentUUID().equals(((CurrentChatItem) listMain.get(position)).getReceiverUUID())){
                return RECIEVER;
            } else {
                return SENDER;
            }
        } else {
            return NULL;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return (listMain != null) ? listMain.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return listMain.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            switch (getItemViewType(i)){
                case SENDER:
                    view = inflater.inflate(R.layout.chat_sender_list_view_item,null);
                    TextView name = (TextView)view.findViewById(R.id.sender_message);
                    name.setText(((CurrentChatItem) listMain.get(i)).getMessage());
                    break;
                case RECIEVER:
                    view = inflater.inflate(R.layout.chat_receiver_list_view_item,null);
                    TextView nameR = (TextView)view.findViewById(R.id.reciever_message);
                    nameR.setText(((CurrentChatItem) listMain.get(i)).getMessage());
                    break;
            }
        }
//        switch (getItemViewType(i)){
//            case ITEM:
//                ImageView iv = (ImageView) view.findViewById(R.id.daily_schedule_list_view_item_iv_image);
//                TextView name = (TextView) view.findViewById(R.id.daily_schedule_list_view_item_tv_name);
//                TextView time = (TextView) view.findViewById(R.id.daily_schedule_list_view_item_tv_time);
//                TextView filler = (TextView) view.findViewById(R.id.filler_text);
//                // name and time
//                if(((DailyScheduleItem) listMain.get(i)).getName() == null){
//                    filler.setVisibility(View.VISIBLE);
//                    filler.setText("No Work Today!");
//                    name.setVisibility(View.INVISIBLE);
//                    time.setVisibility(View.INVISIBLE);
//                    iv.setVisibility(View.INVISIBLE);
//                } else {
//                    filler.setVisibility(View.INVISIBLE);
//                    name.setVisibility(View.VISIBLE);
//                    time.setVisibility(View.VISIBLE);
//                    iv.setVisibility(View.VISIBLE);
//                    Picasso.with(context).load(((DailyScheduleItem) listMain.get(i)).getImage()).into(iv);
//                    name.setText(((DailyScheduleItem) listMain.get(i)).getName());
//                    time.setText(((DailyScheduleItem) listMain.get(i)).getTime());
//                }
//                break;
//            case HEADER:
//                TextView header = (TextView) view.findViewById(R.id.textSeparator);
//                final SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
//                final SimpleDateFormat format = new SimpleDateFormat("EEEE", Locale.US);
//                try {
//                    final Date tempDate = originalFormat.parse((String)listMain.get(i));
//                    String newString = format.format(tempDate);
//                    header.setText(newString);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                break;
//        }
        return view;
    }


}
