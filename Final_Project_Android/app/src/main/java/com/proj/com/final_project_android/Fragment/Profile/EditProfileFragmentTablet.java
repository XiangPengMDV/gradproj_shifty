package com.proj.com.final_project_android.Fragment.Profile;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.proj.com.final_project_android.R;

import java.io.ByteArrayOutputStream;

public class EditProfileFragmentTablet extends Fragment {

    public static final String TAG_Main = "EditProfileFragmentTagTablet";
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    String mCompanyId;
    EditText phone;
    EditText email;
    TextInputLayout work_id;
    TextInputLayout name;
    Button changeImage;
    Bitmap bitToSave;
    StorageReference mStorageRef;
    private static final int REQUEST_IMAGE_CAPTURE = 0x01002;

    //Overrides
    public static Fragment newInstance() {
        Bundle args = new Bundle();
        EditProfileFragmentTablet fragment = new EditProfileFragmentTablet();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.save_profile_menu).setVisible(true);
        menu.findItem(R.id.cancel_profile_menu).setVisible(true);
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        //menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        //menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(false);
        menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_profile_menu:
                saveToFirebaseTablet();
                break;
            case R.id.cancel_profile_menu:
                cancelEditProfileTablet();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_fragment_tablet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        email = (EditText) getActivity().findViewById(R.id.email_tablet_input);
        phone = (EditText) getActivity().findViewById(R.id.phone_tablet_input);
        changeImage = (Button) getActivity().findViewById(R.id.change_picture_button);
        work_id = (TextInputLayout) getActivity().findViewById(R.id.work_id_text_input);
        name = (TextInputLayout) getActivity().findViewById(R.id.name_text_input);
        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });
        pullFromFirebase();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            bitToSave = (Bitmap) extras.get("data");
            updateToFirebaseImage();
            ProfileFragment temp = new ProfileFragment();
            if(temp.isVisible()){
                temp.profileImage.setImageBitmap(bitToSave);
            }
        }
    }



    //My Functions
    private void openCamera(){
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //Device has a normal android camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }
    public void cancelEditProfileTablet(){
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ProfileFragment.newInstance()
        ).commit();
        getFragmentManager().popBackStack();
    }

    //Firebase Func
    public void pullFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
            companyRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("email").getValue(String.class) != null){
                        email.setText(dataSnapshot.child("email").getValue(String.class));
                    } else {
                        email.setText("");
                    }
                    if(dataSnapshot.child("workID").getValue(String.class) != null){
                        work_id.getEditText().setText(dataSnapshot.child("workID").getValue(String.class));
                    } else {
                        work_id.getEditText().setText("");
                    }
                    if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                        phone.setText(dataSnapshot.child("phone-number").getValue(String.class));
                    } else {
                        phone.setText("");
                    }
                    if(dataSnapshot.child("name").getValue(String.class) != null){
                        name.getEditText().setText(dataSnapshot.child("name").getValue(String.class));
                    } else {
                        name.getEditText().setText("");
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //no update
                }
            });
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }
    public void updateToFirebaseImage(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitToSave.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();
            imageRef.putBytes(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    @SuppressWarnings("VisibleForTests") String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                    DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
                    ref.child("image").setValue(downloadUrl);
                    Toast.makeText(getContext(), "Image saved", Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), "Image failed to save", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            updateToFirebaseImage();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }
    public void saveToFirebaseTablet(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            if(email.getText().toString().isEmpty() || phone.getText().toString().isEmpty() || work_id.getEditText().getText().toString().isEmpty() || name.getEditText().getText().toString().isEmpty()){
                //somethings empty
                Toast.makeText(getContext(), "Please make sure no fields are empty", Toast.LENGTH_SHORT).show();
            } else {
                //updating email
                currentUser.updateEmail(email.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
                DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
                ref.child("email").setValue(email.getText().toString());
                ref.child("phone-number").setValue(phone.getText().toString());
                ref.child("workID").setValue(work_id.getEditText().getText().toString());
                ref.child("name").setValue(name.getEditText().getText().toString());
                getFragmentManager().popBackStack();
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        ProfileFragment.newInstance()
                ).commit();
            }
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            saveToFirebaseTablet();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }


}
