package com.proj.com.final_project_android.Helper.ScheduleHelper;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class  DailyScheduleListViewAdapter extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;

    private final Context context;
    private final ArrayList<DailyScheduleItem> daily_employees_list;

    public DailyScheduleListViewAdapter(Context context, ArrayList<DailyScheduleItem> daily_employees_list) {
        this.context = context;
        this.daily_employees_list = daily_employees_list;
    }

    @Override
    public int getCount() {
        return (daily_employees_list != null) ? daily_employees_list.size() : 0;
    }

    @Override
    public DailyScheduleItem getItem(int i) {
        return daily_employees_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.schedule_detail_list_view_item_phone,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // use Picasso to load the image
        if(daily_employees_list.get(i).getImage() != null){
           Picasso.with(context).load(daily_employees_list.get(i).getImage()).into(holder.image);
        }
        // name and time
        holder.name.setText(daily_employees_list.get(i).getName());
        holder.time.setText(daily_employees_list.get(i).getTime());

        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;
        final TextView time;
        final ConstraintLayout cs;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.daily_schedule_list_view_item_iv_image);
            name = (TextView) v.findViewById(R.id.daily_schedule_list_view_item_tv_name);
            time = (TextView) v.findViewById(R.id.daily_schedule_list_view_item_tv_time);
            cs = (ConstraintLayout) v.findViewById(R.id.constraintLayout);
        }
    }
}
