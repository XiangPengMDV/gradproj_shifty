package com.proj.com.final_project_android.Helper.ScheduleHelper;

import android.support.annotation.Nullable;


public class SelectedDayItem {
    String name;
    String shift;
    String image;
    String dateID;
    String uuid;

    public SelectedDayItem(String name, String shift, @Nullable String image, String dateID, String uuid) {
        this.name = name;
        this.shift = shift;
        this.image = image;
        this.dateID = dateID;
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDateID() {
        return dateID;
    }
}
