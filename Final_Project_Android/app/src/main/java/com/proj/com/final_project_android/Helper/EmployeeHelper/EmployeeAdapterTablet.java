package com.proj.com.final_project_android.Helper.EmployeeHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class EmployeeAdapterTablet extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<EmployeeItem> employee_list;
    private Boolean on_edit;

    public EmployeeAdapterTablet(Context context, ArrayList<EmployeeItem> employee_list, Boolean on_edit) {
        this.context = context;
        this.employee_list = employee_list;
        this.on_edit = on_edit;
    }
    public void updateEdit(Boolean edit){
        on_edit = edit;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return (employee_list != null) ? employee_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return employee_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.employees_tablet_grid_view_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if(employee_list.get(i).getImage() != null){
            Picasso.with(context).load(employee_list.get(i).getImage()).into(holder.image);
        }
        holder.name.setText(employee_list.get(i).getName());



        if (on_edit) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setClickable(true);
        } else {
            holder.checkBox.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;
        final CheckBox checkBox;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.employee_list_tablet_image);
            name = (TextView) v.findViewById(R.id.employee_list_tablet_name);
            checkBox = (CheckBox) v.findViewById(R.id.tablet_employee_checkbox);
        }
    }
}
