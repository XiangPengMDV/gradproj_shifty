package com.proj.com.final_project_android.Fragment.Announcements;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;


public class AddAnnouncementFragment extends Fragment {

    TextInputLayout title;
    TextInputLayout body;
    String mCompanyId;
    String mUserName;
    Calendar mCalendar = Calendar.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    //overrides
    public static AddAnnouncementFragment newInstance() {
        Bundle args = new Bundle();
        AddAnnouncementFragment fragment = new AddAnnouncementFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.save_announcement_menu).setVisible(true);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save_announcement_menu){
            saveAnnouncement();
        } else if (item.getItemId() == R.id.cancel_announcement_menu) {
            if(getActivity().getResources().getBoolean(R.bool.isTablet)){
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        AnnouncementFragmentTablet.newInstance()
                ).commit();
            } else {
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        AnnouncementFragmentPhone.newInstance()
                ).commit();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_announcement_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Add Announcement");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Add Announcement");
        }
        super.onActivityCreated(savedInstanceState);
        title = (TextInputLayout) getActivity().findViewById(R.id.add_announcement_title);
        body = (TextInputLayout) getActivity().findViewById(R.id.add_announcement_body);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        pullUserInfo();
    }

    //firebase
    public void pullUserInfo(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network on
        if(isConnected){
            DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mUserName = dataSnapshot.child("name").getValue(String.class);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            //network off
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullUserInfo();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }




    }

    public void saveAnnouncement(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            //network on
            if(title.getEditText().getText().toString().isEmpty() || body.getEditText().getText().toString().isEmpty()){
                Toast.makeText(getContext(), "Please fill in the fields", Toast.LENGTH_SHORT).show();
            } else {
                DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId)
                        .child("announcements").child(UUID.randomUUID().toString());
                ref.child("author").setValue(mUserName);
                ref.child("title").setValue(title.getEditText().getText().toString());
                ref.child("body").setValue(body.getEditText().getText().toString());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                String datePosted = dateFormat.format(mCalendar.getTime());
                ref.child("date").setValue(datePosted);
                if(getActivity().getResources().getBoolean(R.bool.isTablet)){
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction().replace(
                            R.id.core_activity_main_content,
                            AnnouncementFragmentTablet.newInstance()
                    ).commit();
                } else {
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction().replace(
                            R.id.core_activity_main_content,
                            AnnouncementFragmentPhone.newInstance()
                    ).commit();
                }

            }
        } else {
            //newtwork off
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            saveAnnouncement();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }




    }


}
