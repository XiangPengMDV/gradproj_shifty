package com.proj.com.final_project_android.Activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.proj.com.final_project_android.Fragment.Announcements.AnnouncementFragmentPhone;
import com.proj.com.final_project_android.Fragment.Announcements.AnnouncementFragmentTablet;
import com.proj.com.final_project_android.Fragment.Employees.EmployeesFragment;
import com.proj.com.final_project_android.Fragment.Employees.EmployeesFragmentTablet;
import com.proj.com.final_project_android.Fragment.Notifications.NotificationFragment;
import com.proj.com.final_project_android.Fragment.Profile.ProfileFragment;
import com.proj.com.final_project_android.Fragment.Schedule.ScheduleFragment;
import com.proj.com.final_project_android.Fragment.Schedule.ScheduleFragmentPhone;
import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;


public class TabletCoreActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "CoreActivity";
    boolean mTablet;
    FirebaseAuth mAuth;
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    TextView navName;
    TextView navAccessLevel;
    ImageView navImage;
    String mCompanyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.core_activity);
        //assigning toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        mAuth = FirebaseAuth.getInstance();
        companyIdPull();
        //setup Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        //setup Nav/Drawer listener
        NavigationView navigationView = (NavigationView) findViewById(R.id.core_activity_nav_view);
        View header = navigationView.getHeaderView(0);
        navImage = (ImageView) header.findViewById(R.id.activity_start_profile_nav_header_iv_image);
        navName = (TextView) header.findViewById(R.id.activity_start_profile_nav_header_tv_title);
        navAccessLevel = (TextView) header.findViewById(R.id.activity_start_profile_nav_header_tv_description);
        navigationView.setNavigationItemSelectedListener(this);
        SharedPreferences pref = getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        getFirebaseUserInfo();

    }



    @Override
    protected void onStart() {
        super.onStart();
            getFragmentManager().beginTransaction().replace(R.id.core_activity_main_content,
                    ProfileFragment.newInstance()).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        getFragmentManager().popBackStack();
        int id = item.getItemId();
        Fragment selectedFragment = null;
        Class selectedFragmentClass = null;
        switch (id){
            case R.id.core_activity_drawer_list_profile:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = ProfileFragment.class;
                } else {
                    selectedFragmentClass = ProfileFragment.class;
                }

                break;
            case R.id.core_activity_drawer_list_employees:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = EmployeesFragmentTablet.class;
                } else {
                    selectedFragmentClass = EmployeesFragment.class;
                }
                break;
            case R.id.core_activity_drawer_list_schedule:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = ScheduleFragment.class;
                } else {
                    selectedFragmentClass = ScheduleFragmentPhone.class;
                }
                break;
            case R.id.core_activity_drawer_list_announcements:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = AnnouncementFragmentTablet.class;
                } else {
                    selectedFragmentClass = AnnouncementFragmentPhone.class;
                }
                break;
            case R.id.core_activity_drawer_list_chat:
                if(getResources().getBoolean(R.bool.isTablet)) {
                    selectedFragmentClass = NotificationFragment.class;
                } else {
                    selectedFragmentClass = NotificationFragment.class;
                }
                break;
            case R.id.core_activity_drawer_list_logout:
                logoutOfFirebase();
                return true;
        }
        try{
            selectedFragment = (Fragment) selectedFragmentClass.newInstance();
        } catch (Exception e){
            e.printStackTrace();
        }
        getFragmentManager().beginTransaction().replace(R.id.core_activity_main_content,
                selectedFragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    //Firebase Func
    public void companyIdPull(){
        final DatabaseReference ref = database.getReference("Users").child(currentUser.getUid());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                SharedPreferences pref =  getSharedPreferences("pref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(getString(R.string.user_company_key), dataSnapshot.child("company-uuid").getValue(String.class));
                //editor.putString(getString(R.string.user_access_level), dataSnapshot.child("occupation").getValue(String.class));
                editor.apply();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //nothing
            }
        });
    }

    public void setTheDamnTitle(String title){
        getSupportActionBar().setTitle(title);

    }

    public void logoutOfFirebase(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        finish();
        Intent startIntent = new Intent();
        startIntent.setClass(getApplicationContext(), StartActivity.class);
        startIntent.setFlags(startIntent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(startIntent);
        mAuth.signOut();
    }

    //FIREBASE//
    public void getFirebaseUserInfo(){
        final DatabaseReference myRef =  database.getReference("Companies").child(mCompanyId).child("employees")
                .child(currentUser.getUid());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                navName.setText(dataSnapshot.child("name").getValue(String.class));
                navAccessLevel.setText(dataSnapshot.child("occupation").getValue(String.class));
                Picasso.with(getApplicationContext()).load(dataSnapshot.child("image").getValue(String.class)).into(navImage);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}
