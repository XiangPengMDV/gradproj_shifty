package com.proj.com.final_project_android.Fragment.Profile;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;

import java.io.ByteArrayOutputStream;


public class EditProfileFragment extends Fragment implements View.OnClickListener {

    //Firebase
    private static final String TAG = "EditProfileFragment";
    public static final String TAG_Main = "EditProfileFragmentTag";
    StorageReference mStorageRef;
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ImageView editImage;
    TextInputLayout name;
    EditText editPhone;
    EditText editEmail;
    TextInputLayout work_id;
    Button changePassword;
    String mCompanyId;
    TextView infoText;
    Bitmap bitToSave;
    private static final int REQUEST_IMAGE_CAPTURE = 0x01002;
    private static final int RESULT_OK = 0x01001;

    //Overrides
    public static EditProfileFragment newInstance() {
        Bundle args = new Bundle();
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    //enabling access to appbar
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(true);
        menu.findItem(R.id.cancel_profile_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.cancel_profile_menu:
                cancelEditProfile();
                break;
            case R.id.save_profile_menu:
                saveEditProfile();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Edit Profile");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Edit Profile");
        }
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");

        editEmail = (EditText) getActivity().findViewById(R.id.edit_email);
        editPhone = (EditText) getActivity().findViewById(R.id.edit_phone_number);
        work_id = (TextInputLayout) getActivity().findViewById(R.id.work_id_text_input_phone);
        name = (TextInputLayout) getActivity().findViewById(R.id.name_text_input_phone);

        editImage = (ImageView) getActivity().findViewById(R.id.edit_profile_image);
        changePassword = (Button) getActivity().findViewById(R.id.change_password_button);
        infoText = (TextView) getActivity().findViewById(R.id.info_text);
        changePassword.setOnClickListener(this);
        infoText.setOnClickListener(this);
        editImage.setOnClickListener(this);

        pullFromFirebase();

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.change_password_button){
            //change password (Later)
            showChangePasswordDialog();
        } else if (view.getId() == R.id.edit_profile_image || view.getId() == R.id.info_text){
            openCamera();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            bitToSave = (Bitmap) extras.get("data");
            saveImageToFirebase();
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    //My Functions
    private void openCamera(){
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //Device has a normal android camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }
    public void saveEditProfile(){
        saveToFireBase();
//        getFragmentManager().popBackStack();
//        getFragmentManager().beginTransaction().replace(
//                R.id.core_activity_main_content,
//                ProfileFragment.newInstance()
//        ).commit();
    }
    public void cancelEditProfile(){
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ProfileFragment.newInstance()
        ).commit();
    }


    //Firebase Funcs
    public void pullFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //Network found
        if(isConnected){

            final StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
            final long ONE_MEGABYTE = 1024 * 1024;

            //mCompanyId = dataSnapshot.child("company-uuid").getValue(String.class);
            final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
            companyRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("name").getValue(String.class) != null){
                        name.getEditText().setText(dataSnapshot.child("name").getValue(String.class));
                    } else {
                        name.getEditText().setText("");
                    }
                    if(dataSnapshot.child("email").getValue(String.class) != null){
                        editEmail.setText(dataSnapshot.child("email").getValue(String.class));
                    } else {
                        editEmail.setText("");
                    }
                    if(dataSnapshot.child("workID").getValue(String.class) != null){
                        work_id.getEditText().setText(dataSnapshot.child("workID").getValue(String.class));
                    } else {
                        work_id.getEditText().setText("");
                    }
                    if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                        editPhone.setText(dataSnapshot.child("phone-number").getValue(String.class));
                    }
                    imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            editImage.setImageBitmap(bmp);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if(getActivity() != null){
                                Toast.makeText(getActivity(), "No Profile Picture found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            //no update
                        }
                    });
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }
    //Saving image
    public void  saveImageToFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitToSave.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();
            imageRef.putBytes(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    @SuppressWarnings("VisibleForTests") String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                    DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
                    ref.child("image").setValue(downloadUrl);
                    Toast.makeText(getContext(), "Image saved", Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), "Image failed to save", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            saveImageToFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }
    //Saving everything but image
    public void saveToFireBase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected) {
            if(editEmail.getText().toString().isEmpty() || editPhone.getText().toString().isEmpty()){
                //something is empty
                Toast.makeText(getContext(), "Please make sure no fields are empty", Toast.LENGTH_SHORT).show();

            } else {
                //updating email
                currentUser.updateEmail(editEmail.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "onSuccess: ");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: ");
                    }
                });
                //updating phone number (and image later)
                DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
                ref.child("email").setValue(editEmail.getText().toString());
                ref.child("phone-number").setValue(editPhone.getText().toString());
                ref.child("name").setValue(name.getEditText().getText().toString());
                ref.child("workID").setValue(work_id.getEditText().getText().toString());
                getFragmentManager().popBackStack();
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        ProfileFragment.newInstance()
                ).commit();
            }
        } else {
//no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            saveToFireBase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }
    //changing password
    private void showChangePasswordDialog(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //Network found
        if(isConnected){
            AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater customInflator = getActivity().getLayoutInflater();
            final View dialogView = customInflator.inflate(R.layout.edit_profile_change_password_dialog, null);
            newPasswordDialogBuilder.setView(dialogView);
            final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
            newPasswordDialogBuilder.setTitle("Change Password");
            newPasswordDialogBuilder.setMessage("Please Enter your old Password then your new one.(At least 6 Characters)");
            newPasswordDialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //change password in firebase
                    if(newPasswordET.getText().toString().isEmpty()){
                        //one of the fields is empty
                        Toast.makeText(getContext(), "No Password Entered, Please try again.", Toast.LENGTH_LONG).show();
                    } else if (newPasswordET.getText().toString().length() < 6) {
                        //password too short
                        Toast.makeText(getContext(), "New Password was not atleast 6 characters, Please try again.", Toast.LENGTH_LONG).show();
                    } else {
                        //all good change password
                        FirebaseAuth.getInstance().getCurrentUser().updatePassword(newPasswordET.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getContext(), "Password Updated!", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                //failed
                                Toast.makeText(getContext(), "Failed to change password in firebase", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                }
            });
            newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing
                }
            });
            AlertDialog mainAlert = newPasswordDialogBuilder.create();
            mainAlert.show();
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            showChangePasswordDialog();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }

}
