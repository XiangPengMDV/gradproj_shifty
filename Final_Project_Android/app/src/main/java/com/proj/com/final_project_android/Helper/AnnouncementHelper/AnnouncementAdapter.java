package com.proj.com.final_project_android.Helper.AnnouncementHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.proj.com.final_project_android.R;

import java.util.ArrayList;


public class AnnouncementAdapter extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<AnnouncementItem> announcement_list;
    private Boolean on_edit;

    public AnnouncementAdapter(Context context, ArrayList<AnnouncementItem> employee_list, Boolean on_edit) {
        this.context = context;
        this.announcement_list = employee_list;
        this.on_edit = on_edit;
    }
    public void updateEdit(Boolean edit){
        on_edit = edit;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (announcement_list != null) ? announcement_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return announcement_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.announcement_list_view_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.title.setText(announcement_list.get(i).getTitle());
        holder.date.setText(announcement_list.get(i).getDatePosted());


        if (on_edit) {
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    static class ViewHolder {
        final TextView title;
        final TextView date;
        final CheckBox checkBox;

        ViewHolder(View v) {
            title = (TextView) v.findViewById(R.id.announcement_item_title);
            date = (TextView) v.findViewById(R.id.announcement_item_date);
            checkBox = (CheckBox) v.findViewById(R.id.announcement_checkbox);
        }
    }
}
