package com.proj.com.final_project_android.Fragment.Notifications;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;

import java.util.ArrayList;


public class NotificationFragment extends Fragment {

    ListView mList;
    ArrayList<String> notifications;
    String mCompanyId;
    ArrayAdapter<String> mAdapter;
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static NotificationFragment newInstance() {
        
        Bundle args = new Bundle();
        
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_layout, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Notifications");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Notifications");
        }
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        notifications = new ArrayList<>();
        mAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, notifications);
        mList = (ListView) getActivity().findViewById(R.id.notification_list);
        mList.setAdapter(mAdapter);
        pullNotifications();
        super.onActivityCreated(savedInstanceState);
    }

    public void pullNotifications(){
        DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("notifications");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                notifications.add(dataSnapshot.child("notificationText").getValue(String.class));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }





}
