package com.proj.com.final_project_android.Fragment.Schedule;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.Helper.ScheduleHelper.AddScheduleEmployeeAdapter;
import com.proj.com.final_project_android.Helper.ScheduleHelper.AddScheduleEmployeeItem;
import com.proj.com.final_project_android.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

public class AddScheduleFragment extends Fragment implements AdapterView.OnItemClickListener, CalendarView.OnDateChangeListener{
    private static final String TAG = "AddScheduleFragment";
    private ListView employeeList;
    private ArrayList<AddScheduleEmployeeItem> mList;
    String mCompanyId;
    String selectedDate;
    String selectedShiftStart;
    String selectedShiftEnd;
    String selectedImage;
    String selectedUUID;
    Calendar mCalendar = Calendar.getInstance();
    private AddScheduleEmployeeAdapter mAdapter;
    String selectedEmployee;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    CalendarView mainCalendar;
    TimePicker shiftStart;
    TimePicker shiftEnd;

    public static AddScheduleFragment newInstance() {

        Bundle args = new Bundle();

        AddScheduleFragment fragment = new AddScheduleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.save_schedule_menu).setVisible(true);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save_schedule_menu){
            saveToSchedule();
        } else if (item.getItemId() == R.id.cancel_schedule_menu){
            cancelAddSchedule();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_schedule_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Add To Schedule");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Add To Schedule");
        }
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        employeeList = (ListView) getActivity().findViewById(R.id.add_schedule_employee_list);
        mainCalendar = (CalendarView) getActivity().findViewById(R.id.add_schedule_calendar);
        shiftStart = (TimePicker) getActivity().findViewById(R.id.shift_start);
        shiftEnd = (TimePicker) getActivity().findViewById(R.id.shift_end);
        shiftStart.setHour(12);
        shiftStart.setMinute(0);
        shiftEnd.setHour(12);
        shiftEnd.setMinute(0);
        selectedShiftStart = String.format(Locale.US, "%02d:%02d", shiftStart.getHour(), shiftStart.getMinute());
        selectedShiftEnd = String.format(Locale.US, "%02d:%02d", shiftEnd.getHour(), shiftEnd.getMinute());
        employeeList.setSelector(R.color.colorPrimary);
        mList = new ArrayList<>();
        mAdapter = new AddScheduleEmployeeAdapter(getContext(), mList, false);
        employeeList.setAdapter(mAdapter);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        employeeList.setOnItemClickListener(this);
        mainCalendar.setOnDateChangeListener(this);
        shiftStart.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                selectedShiftStart = String.format(Locale.US, "%02d:%02d", timePicker.getHour(), timePicker.getMinute());

            }
        });
        shiftEnd.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                selectedShiftEnd = String.format(Locale.US, "%02d:%02d", timePicker.getHour(), timePicker.getMinute());
            }
        });
        selectedDate = dateFormat.format(mCalendar.getTime());
        pullEmployeesForScheduleFromFirebase();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedEmployee = mList.get(i).getName();
        selectedImage = mList.get(i).getImage();
        selectedUUID = mList.get(i).getUuid();
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
        mCalendar.set(i, i1, i2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        selectedDate = dateFormat.format(mCalendar.getTime());
    }

    //My func
    public void cancelAddSchedule(){
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ScheduleFragment.newInstance()
        ).commit();
    }
    //Firebase
    public void pullEmployeesForScheduleFromFirebase(){
        //company id was already found
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mList.add(new AddScheduleEmployeeItem(dataSnapshot.child("name").getValue(String.class),
                        dataSnapshot.child("image").getValue(String.class),
                        dataSnapshot.getKey(),
                        dataSnapshot.child("occupation").getValue(String.class),
                        dataSnapshot.child("Sunday").getValue(Boolean.class),
                        dataSnapshot.child("Monday").getValue(Boolean.class),
                        dataSnapshot.child("Tuesday").getValue(Boolean.class),
                        dataSnapshot.child("Wednesday").getValue(Boolean.class),
                        dataSnapshot.child("Thursday").getValue(Boolean.class),
                        dataSnapshot.child("Friday").getValue(Boolean.class),
                        dataSnapshot.child("Saturday").getValue(Boolean.class)));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
    public void saveToSchedule(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            if(selectedEmployee != null){

                String uuid = UUID.randomUUID().toString();
                Log.d(TAG, "saveToSchedule: " + selectedShiftStart);
                DatabaseReference ref = database.getReference().child("Companies")
                        .child(mCompanyId).child("schedule")
                        .child(selectedDate)
                        .child(selectedUUID);
                DatabaseReference notificationRef = database.getReference().child("Companies")
                        .child(mCompanyId).child("employees")
                        .child(selectedUUID).child("notifications").child(UUID.randomUUID().toString());


                ref.child("shift-start").setValue(selectedShiftStart);
                ref.child("shift-end").setValue(selectedShiftEnd);
                ref.child("name").setValue(selectedEmployee);
                ref.child("image").setValue(selectedImage);
                ref.child("uuid").setValue(selectedUUID);

                notificationRef.child("notificationText").setValue("You have been added to the schedule for " + selectedDate);
                notificationRef.child("read").setValue(false);
                notificationRef.child("notified").setValue(false);

                Toast.makeText(getContext(), "Successfully Added to Schedule", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Please Select Employee", Toast.LENGTH_SHORT).show();
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            saveToSchedule();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }
}
