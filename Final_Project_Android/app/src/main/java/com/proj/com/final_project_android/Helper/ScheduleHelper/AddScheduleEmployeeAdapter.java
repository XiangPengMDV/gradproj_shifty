package com.proj.com.final_project_android.Helper.ScheduleHelper;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AddScheduleEmployeeAdapter extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<AddScheduleEmployeeItem> employee_list;
    private Boolean on_edit;

    public AddScheduleEmployeeAdapter(Context context, ArrayList<AddScheduleEmployeeItem> employee_list, Boolean on_edit) {
        this.context = context;
        this.employee_list = employee_list;
        this.on_edit = on_edit;
    }
    public void updateEdit(Boolean edit){
        on_edit = edit;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return (employee_list != null) ? employee_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return employee_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.add_schedule_employee_list,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if(employee_list.get(i).getImage() != null){
            Picasso.with(context).load(employee_list.get(i).getImage()).into(holder.image);
        }
        holder.name.setText(employee_list.get(i).getName());
        //sunday
        if(employee_list.get(i).getSunday()!=null){
            if(employee_list.get(i).getSunday()){
                //T
                holder.sunday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if (!(employee_list.get(i).getSunday())){
                //F
                //F = they need that day off(Red)
                holder.sunday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }
        if(employee_list.get(i).getMonday()!=null){
//monday
            if (employee_list.get(i).getMonday()){
                holder.monday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if (!(employee_list.get(i).getMonday())){
                holder.monday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }
        if(employee_list.get(i).getTuesday()!=null){

            //tuesday
            if (employee_list.get(i).getTuesday()){
                holder.tuesday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if(!(employee_list.get(i).getTuesday())){
                holder.tuesday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }
        if(employee_list.get(i).getWednesday()!=null){

            //wednesday
            if (employee_list.get(i).getWednesday()){
                holder.wednesday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if (!(employee_list.get(i).getWednesday())){
                holder.wednesday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }
        if(employee_list.get(i).getThursday()!=null){
//thursday
            if (employee_list.get(i).getThursday()){
                holder.thursday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if (!(employee_list.get(i).getThursday())){
                holder.thursday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }
        if(employee_list.get(i).getFriday()!=null){
//friday
            if(employee_list.get(i).getFriday()){
                holder.friday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if (!(employee_list.get(i).getFriday())){
                holder.friday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }
        if(employee_list.get(i).getSaturday()!=null){
            //saturday
            if(employee_list.get(i).getSaturday()){
                holder.saturday.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else if (!(employee_list.get(i).getSaturday())){
                holder.saturday.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
        }








        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;
        final TextView sunday;
        final TextView monday;
        final TextView tuesday;
        final TextView wednesday;
        final TextView thursday;
        final TextView friday;
        final TextView saturday;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.add_schedule_employee_image);
            name = (TextView) v.findViewById(R.id.add_schedule_employee_name);
            sunday = (TextView) v.findViewById(R.id.add_schedule_S);
            monday = (TextView) v.findViewById(R.id.add_schedule_M);
            tuesday = (TextView)v.findViewById(R.id.add_schedule_T);
            wednesday = (TextView)v.findViewById(R.id.add_schedule_W);
            thursday = (TextView)v.findViewById(R.id.add_schedule_TH);
            friday = (TextView)v.findViewById(R.id.add_schedule_F);
            saturday = (TextView)v.findViewById(R.id.add_schedule_S2);
        }
    }
}
