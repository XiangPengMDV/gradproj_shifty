package com.proj.com.final_project_android.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.proj.com.final_project_android.Fragment.Start.StartFragment;
import com.proj.com.final_project_android.R;

public class StartActivity extends AppCompatActivity {
    /*
    Step by Step
    1. Connect to Firebase with the Firebase Helper (Tools>Firebase)
        This adds the necessary class paths and
        plugins into gradle automatically.
    2. Added Firebase authentication to App Gradle
        (compile 'com.google.firebase:firebase-auth:10.0.1')
        Also must set up a sign in method on Firebase Console side
        or it wont have anything to connect to.
    3. Declare FirebaseAuth and AuthStateListener globally (private)
    4. Initialize FirebaseAuth instance
    5. Initialize AuthStateListener to be able to check if someone
        recently logged in on device
    6. Attaching listener to FirebaseAuth in onStart and removing in onStop
    7. Create dimen.xml for values/values-large/values-xlarge with a
        bool for determining if it is tablet or not and change orientation accordingly
        (Right click res>new>Android Resource Directory>Size Qualifier)
    8. Make sure to create landscape and portrait specific layouts for auto changing
     */
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Checking device (and assigning orientation)
        checkForDevice();
        setContentView(R.layout.start_activity);
        //Assigning fragment
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(
                R.id.start_activity_container,
                StartFragment.newInstance()
        ).commit();
        //FirebaseAuth and AuthListener Initialization
        mAuth = FirebaseAuth.getInstance();
        //This will check if a user is logged in or not
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //User pulled from firebase
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user!=null){
                    if(checkForDevice()){
                        //user exists meaning logged in
                        Intent coreIntent = new Intent();
                        coreIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        coreIntent.setClass(getApplicationContext(), TabletCoreActivity.class);
                        startActivity(coreIntent);
                        finish();
                    } else {
                        //user exists meaning logged in
                        Intent coreIntent = new Intent();
                        coreIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        coreIntent.setClass(getApplicationContext(), CoreActivity.class);
                        startActivity(coreIntent);
                        finish();
                    }

                } else {
                    //user is null meaning logged out so let this page run normally
                }
            }
        };

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener!=null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    //Func for checkign whether phone or tablet and changing orientation accordingly
    public Boolean checkForDevice(){
        //boolean determining tablet or not
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if (isTablet) {
            //Current Device is Tablet
            //setting orientation to landscape
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            //Current Device is Phone
            //setting orientation to phone
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        return isTablet;
    }




}
