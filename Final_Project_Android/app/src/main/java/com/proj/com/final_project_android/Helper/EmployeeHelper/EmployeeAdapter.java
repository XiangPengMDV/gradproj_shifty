package com.proj.com.final_project_android.Helper.EmployeeHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class EmployeeAdapter extends BaseAdapter {

    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<EmployeeItem> employee_list;
    private Boolean on_edit;

    public EmployeeAdapter(Context context, ArrayList<EmployeeItem> employee_list, Boolean on_edit) {
        this.context = context;
        this.employee_list = employee_list;
        this.on_edit = on_edit;
    }
    public void updateEdit(Boolean edit){
            on_edit = edit;
            notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (employee_list != null) ? employee_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return employee_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.employees_grid_view_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if(employee_list.get(i).getName().length() > 7){
            String sub = employee_list.get(i).getName().substring(0,6) + "..";
            holder.name.setText(sub);
        } else {
            holder.name.setText(employee_list.get(i).getName());
        }
        if(employee_list.get(i).getImage() != null){
            Picasso.with(context).load(employee_list.get(i).getImage())
                    .into(holder.image);
        } else {

        }
        if(employee_list.get(i).getAccess() != null){
            holder.accessLevel.setText(employee_list.get(i).getAccess());
        }

        if (on_edit) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    3.0f
            );
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.name.setLayoutParams(param);
            holder.checkBox.setClickable(true);
        } else {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    4.0f
            );
            holder.name.setLayoutParams(param);
            holder.checkBox.setVisibility(View.GONE);
        }



        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;
        final TextView accessLevel;
        final CheckBox checkBox;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.employee_item_image);
            name = (TextView) v.findViewById(R.id.announcement_item_title);
            accessLevel = (TextView) v.findViewById(R.id.announcement_item_date);
            checkBox = (CheckBox) v.findViewById(R.id.employee_on_edit_checkbox);
        }
    }
}
