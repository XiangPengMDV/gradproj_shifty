package com.proj.com.final_project_android.Fragment.Announcements;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.Helper.AnnouncementHelper.AnnouncementAdapter;
import com.proj.com.final_project_android.Helper.AnnouncementHelper.AnnouncementItem;
import com.proj.com.final_project_android.R;

import java.util.ArrayList;


public class AnnouncementFragmentPhone extends Fragment implements AdapterView.OnItemClickListener{

    ListView announcementList;
    ArrayList<AnnouncementItem> mList;
    private ArrayList<AnnouncementItem> deleteArray;
    AnnouncementAdapter mAdapter;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private Boolean mEdit = false;
    Menu mMenu;


    //Overrides
    public static AnnouncementFragmentPhone newInstance() {
        Bundle args = new Bundle();
        AnnouncementFragmentPhone fragment = new AnnouncementFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.new_message_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(true);
        menu.findItem(R.id.announcement_edit_menu).setVisible(true);
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        //menu.findItem(R.id.announcement_add_menu).setVisible(false);
        //menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.announcement_add_menu:
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        AddAnnouncementFragment.newInstance()
                ).addToBackStack(null).commit();
                break;
            case R.id.announcement_edit_menu:
                editAnnouncements();
                break;
            case R.id.save_announcement_menu:
                deleteAnnouncements();
                break;
            case R.id.cancel_announcement_menu:
                cancelAnnouncements();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.announcement_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Announcements");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Announcements");
        }
        announcementList = (ListView) getActivity().findViewById(R.id.announcement_list);
        mList = new ArrayList<>();
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        mAdapter = new AnnouncementAdapter(getActivity(), mList, mEdit);
        announcementList.setAdapter(mAdapter);
        announcementList.setOnItemClickListener(this);
        pullAnnouncements();


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mEdit){
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.announcement_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                //mList.add(deleteArray.get(i));
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
                //mList.remove(deleteArray.get(i));
            }
        } else {
            Bundle detailBundle = new Bundle();
            AnnouncementDetailFragmentPhone fragment = AnnouncementDetailFragmentPhone.newInstance();
            detailBundle.putString("announcementCompanyStringDetail", mCompanyId);
            detailBundle.putString("announcementID", mList.get(i).getUuid());
            fragment.setArguments(detailBundle);
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    fragment
            ).addToBackStack(null).commit();
        }
    }


    //My Functions
    public void editAnnouncements(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Edit Announcements");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Edit Announcements");
        }
        deleteArray = new ArrayList<>();
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.announcement_add_menu).setVisible(false);
        mMenu.findItem(R.id.announcement_edit_menu).setVisible(false);
        mMenu.findItem(R.id.save_announcement_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_announcement_menu).setVisible(true);
    }
    public void cancelAnnouncements(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Announcements");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Announcements");
        }
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.announcement_add_menu).setVisible(true);
        mMenu.findItem(R.id.announcement_edit_menu).setVisible(true);
        mMenu.findItem(R.id.save_announcement_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_announcement_menu).setVisible(false);
    }


    //Firebase
    public void pullAnnouncements(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            //network found
            DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements");
            ref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mList.add(new AnnouncementItem(dataSnapshot.child("title").getValue(String.class), dataSnapshot.child("date").getValue(String.class), dataSnapshot.getKey()));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mList = new ArrayList<AnnouncementItem>();
                            pullAnnouncements();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }


    }
    public void deleteAnnouncements(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            //network found
            for(AnnouncementItem announcement: deleteArray){
                final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements")
                        .child(announcement.getUuid());
                ref.removeValue();
            }
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    AnnouncementFragmentPhone.newInstance()
            ).commit();
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteAnnouncements();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();

        }

    }
}
