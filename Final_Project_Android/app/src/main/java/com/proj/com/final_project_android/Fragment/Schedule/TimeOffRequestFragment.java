package com.proj.com.final_project_android.Fragment.Schedule;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;

public class TimeOffRequestFragment extends Fragment implements View.OnClickListener {


    ConstraintLayout sundayC;
    ConstraintLayout mondayC;
    ConstraintLayout tuesdayC;
    ConstraintLayout wednesdayC;
    ConstraintLayout thursdayC;
    ConstraintLayout fridayC;
    ConstraintLayout saturdayC;
    TextView sundayT;
    TextView mondayT;
    TextView tuesdayT;
    TextView wednesdayT;
    TextView thursdayT;
    TextView fridayT;
    TextView saturdayT;
    Button updateButton;
    String mCompanyId;


    public static TimeOffRequestFragment newInstance() {
        Bundle args = new Bundle();
        TimeOffRequestFragment fragment = new TimeOffRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.cancel_time_off_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.cancel_time_off_menu:
                cancelHit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.time_off_request_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Weekly Requests");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Weekly Requests");
        }
        super.onActivityCreated(savedInstanceState);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        sundayC = (ConstraintLayout) getActivity().findViewById(R.id.sunday_container);
        mondayC = (ConstraintLayout) getActivity().findViewById(R.id.monday_container);
        tuesdayC = (ConstraintLayout) getActivity().findViewById(R.id.tuesday_container);
        wednesdayC = (ConstraintLayout) getActivity().findViewById(R.id.wednesday_container);
        thursdayC = (ConstraintLayout) getActivity().findViewById(R.id.thursday_container);
        fridayC = (ConstraintLayout) getActivity().findViewById(R.id.friday_container);
        saturdayC = (ConstraintLayout) getActivity().findViewById(R.id.saturday_container);
        sundayT = (TextView) getActivity().findViewById(R.id.sunday_text);
        mondayT = (TextView) getActivity().findViewById(R.id.monday_text);
        tuesdayT = (TextView) getActivity().findViewById(R.id.tuesday_text);
        wednesdayT = (TextView) getActivity().findViewById(R.id.wednesday_text);
        thursdayT = (TextView) getActivity().findViewById(R.id.thursday_text);
        fridayT = (TextView) getActivity().findViewById(R.id.friday_text);
        saturdayT = (TextView) getActivity().findViewById(R.id.saturday_text);
        updateButton = (Button) getActivity().findViewById(R.id.time_off_update);
        sundayC.setOnClickListener(this);
        mondayC.setOnClickListener(this);
        tuesdayC.setOnClickListener(this);
        wednesdayC.setOnClickListener(this);
        thursdayC.setOnClickListener(this);
        fridayC.setOnClickListener(this);
        saturdayC.setOnClickListener(this);
        updateButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sunday_container:
                clickOccurred(sundayT, sundayC);
                break;
            case R.id.monday_container:
                clickOccurred(mondayT, mondayC);
                break;
            case R.id.tuesday_container:
                clickOccurred(tuesdayT, tuesdayC);
                break;
            case R.id.wednesday_container:
                clickOccurred(wednesdayT, wednesdayC);
                break;
            case R.id.thursday_container:
                clickOccurred(thursdayT, thursdayC);
                break;
            case R.id.friday_container:
                clickOccurred(fridayT, fridayC);
                break;
            case R.id.saturday_container:
                clickOccurred(saturdayT, saturdayC);
                break;
            case R.id.time_off_update:
                updateTimeOff();
                break;
        }
    }

    public void clickOccurred(TextView textView, ConstraintLayout constraintLayout){
        ColorDrawable backgroundColor = (ColorDrawable) constraintLayout.getBackground();
        if(backgroundColor.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
            constraintLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        } else if (backgroundColor.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
            constraintLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        }
    }

    public void cancelHit(){
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ScheduleFragmentPhone.newInstance()
        ).commit();
    }

    //Firebase
    public void updateTimeOff(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference().child("Companies")
                    .child(mCompanyId).child("employees").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            //Sunday
            ColorDrawable colorToCheck = (ColorDrawable) sundayC.getBackground();
            if(colorToCheck.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Sunday").setValue(false);
            } else if (colorToCheck.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Sunday").setValue(true);
            }
            //Monday
            ColorDrawable colorToCheck1 = (ColorDrawable) mondayC.getBackground();
            if(colorToCheck1.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Monday").setValue(false);
            } else if (colorToCheck1.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Monday").setValue(true);
            }
            //Tuesday
            ColorDrawable colorToCheck2 = (ColorDrawable) tuesdayC.getBackground();
            if(colorToCheck2.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Tuesday").setValue(false);
            } else if (colorToCheck2.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Tuesday").setValue(true);
            }
            //Wednesday
            ColorDrawable colorToCheck3 = (ColorDrawable) wednesdayC.getBackground();
            if(colorToCheck3.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Wednesday").setValue(false);
            } else if (colorToCheck3.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Wednesday").setValue(true);
            }
            //Thursday
            ColorDrawable colorToCheck4 = (ColorDrawable) thursdayC.getBackground();
            if(colorToCheck4.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Thursday").setValue(false);
            } else if (colorToCheck4.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Thursday").setValue(true);
            }
            //Friday
            ColorDrawable colorToCheck5 = (ColorDrawable) fridayC.getBackground();
            if(colorToCheck5.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Friday").setValue(false);
            } else if (colorToCheck5.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Friday").setValue(true);
            }
            //Saturday
            ColorDrawable colorToCheck6 = (ColorDrawable) saturdayC.getBackground();
            if(colorToCheck6.getColor() == ContextCompat.getColor(getContext(), R.color.colorPrimary)){
                ref.child("Saturday").setValue(false);
            } else if (colorToCheck6.getColor() == ContextCompat.getColor(getContext(), R.color.white)){
                ref.child("Saturday").setValue(true);
            }

            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    ScheduleFragmentPhone.newInstance()
            ).commit();

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            updateTimeOff();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }


}
