package com.proj.com.final_project_android.Helper.ScheduleHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SelectedDayAdapter extends BaseAdapter {

    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<SelectedDayItem> day_list;
    private Boolean on_edit;

    public SelectedDayAdapter(Context context, ArrayList<SelectedDayItem> day_list, Boolean on_edit) {
        this.context = context;
        this.day_list = day_list;
        this.on_edit = on_edit;
    }
    public void updateEdit(Boolean edit){
        on_edit = edit;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (day_list != null) ? day_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return day_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.selected_day_list_view_item,
                            viewGroup,
                            false
                    );
            holder = new SelectedDayAdapter.ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if(day_list.get(i).getImage() != null){
            Picasso.with(context).load(day_list.get(i).getImage()).into(holder.image);
        } else {
            holder.image.setImageDrawable(context.getDrawable(R.drawable.shifty_logo));
        }
        holder.name.setText(day_list.get(i).getName());
        holder.shift.setText(day_list.get(i).getShift());


        if (on_edit) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setClickable(true);
        } else {
            holder.checkBox.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;
        final TextView shift;
        final CheckBox checkBox;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.selected_day_image);
            name = (TextView) v.findViewById(R.id.selected_day_name);
            shift = (TextView) v.findViewById(R.id.selected_day_shift);
            checkBox = (CheckBox) v.findViewById(R.id.schedule_selected_checkbox);
        }
    }

}
