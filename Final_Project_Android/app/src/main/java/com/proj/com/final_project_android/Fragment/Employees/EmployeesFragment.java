package com.proj.com.final_project_android.Fragment.Employees;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.Helper.EmployeeHelper.EmployeeAdapter;
import com.proj.com.final_project_android.Helper.EmployeeHelper.EmployeeItem;
import com.proj.com.final_project_android.R;

import java.util.ArrayList;


public class EmployeesFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = "EmployeesFragment";
    private GridView mGrid;
    private EmployeeAdapter mAdapter;
    private ArrayList<EmployeeItem> mList;
    private ArrayList<EmployeeItem> deleteArray;
    private Boolean mEdit = false;
    private Menu mMenu;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    //overrides
    public static EmployeesFragment newInstance() {
        Bundle args = new Bundle();
        EmployeesFragment fragment = new EmployeesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employees_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Employees");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Employees");
        }
        super.onActivityCreated(savedInstanceState);
        mGrid = (GridView) getActivity().findViewById(R.id.employees_grid_view);
        mList = new ArrayList<>();
        deleteArray = new ArrayList<>();
        mAdapter = new EmployeeAdapter(getActivity(), mList, mEdit);
        mGrid.setAdapter(mAdapter);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        mGrid.setOnItemClickListener(this);
        pullEmployeesFromFirebase();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "onItemClick: " + mEdit);
        if(mEdit){
            //editing on
            Log.d(TAG, "onItemClick: ");
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.employee_on_edit_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                //mList.add(deleteArray.get(i));
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
                //mList.remove(deleteArray.get(i));
            }
        } else {
            Log.d(TAG, "onItemClick: ");
            //editing off
            Bundle detailBundle = new Bundle();
            DetailFragment fragment = DetailFragment.newInstance();
            detailBundle.putString("listId", mList.get(i).getUuid());
            fragment.setArguments(detailBundle);
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    fragment
            ).addToBackStack(null).commit();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.new_message_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(true);
        menu.findItem(R.id.add_employee_menu).setVisible(true);
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        //menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        //menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(false);
        menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: ");
        switch (item.getItemId()){
            case R.id.add_employee_menu:
                showAddEmployee();
                break;
            case R.id.edit_employee_menu:
                editEmployees();
                break;
            case R.id.save_edit_employee_menu:
                deleteSelectedEmployees();
                break;
            case R.id.cancel_edit_employee_menu:
                cancelEditEmployees();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    //My functions
    public void showAddEmployee(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AddEmployee.newInstance()
        ).addToBackStack(null).commit();
    }
    public void cancelEditEmployees(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Employees");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Employees");
        }
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.add_employee_menu).setVisible(true);
    }
    public void editEmployees(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Edit Employees");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Edit Employees");
        }
        mEdit = true;
        mAdapter.updateEdit(true);

        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.add_employee_menu).setVisible(false);

    }


    //Firebase Func
    public void pullEmployeesFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            //company id was already found
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees");
            ref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mList.add(new EmployeeItem(dataSnapshot.child("name").getValue(String.class),
                            dataSnapshot.child("image").getValue(String.class), dataSnapshot.getKey(), dataSnapshot.child("occupation").getValue(String.class)));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullEmployeesFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }


    }
    public void deleteSelectedEmployees(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            for(EmployeeItem employee: deleteArray){
                final DatabaseReference userToDelete = database.getReference()
                        .child("Companies").child(mCompanyId).child("employees").child(employee.getUuid());
                final DatabaseReference userBranch = database.getReference().child("Users").child(employee.getUuid());
                userBranch.removeValue();
                userToDelete.removeValue();
            }
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    EmployeesFragment.newInstance()
            ).commit();
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteSelectedEmployees();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }

}
