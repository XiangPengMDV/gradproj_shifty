package com.proj.com.final_project_android.Fragment.Employees;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;


public class DetailFragment extends Fragment {
    ImageView image;
    TextView workId;
    TextView name;
    TextView accessLevel;
    TextView email;
    TextView phoneNumber;
    String mCompanyId;
    String mUUID;
    private StorageReference mStorageRef;
    FirebaseDatabase database = FirebaseDatabase.getInstance();


    //Overrides
    public static DetailFragment newInstance() {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employee_detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        image = (ImageView) getActivity().findViewById(R.id.employee_image);
        workId = (TextView) getActivity().findViewById(R.id.employee_work_id);
        name = (TextView) getActivity().findViewById(R.id.employee_name);
        accessLevel = (TextView) getActivity().findViewById(R.id.employee_access_level);
        email = (TextView) getActivity().findViewById(R.id.employee_email);
        phoneNumber = (TextView) getActivity().findViewById(R.id.employee_phone_number);
        mUUID = getArguments().getString("listId");
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        pullEmployeeDetailFromFirebase();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //Firebase
    public void pullEmployeeDetailFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //network found
        if(isConnected){
            final StorageReference imageRef = mStorageRef.child("profile_images/" + mUUID);
            final long ONE_MEGABYTE = 1024 * 1024;
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees").child(mUUID);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //update ui
                    if(dataSnapshot.child("name").getValue(String.class) != null){
                        name.setText(dataSnapshot.child("name").getValue(String.class));
                        if(getResources().getBoolean(R.bool.isTablet)){
                            ((TabletCoreActivity)getActivity()).setTheDamnTitle(dataSnapshot.child("name").getValue(String.class));
                        } else {
                            ((CoreActivity)getActivity()).setTheDamnTitle(dataSnapshot.child("name").getValue(String.class));
                        }
                    }
                    if(dataSnapshot.child("email").getValue(String.class) != null){
                        email.setText(dataSnapshot.child("email").getValue(String.class));
                    }
                    if(dataSnapshot.child("occupation").getValue(String.class) != null){
                        accessLevel.setText(dataSnapshot.child("occupation").getValue(String.class));
                    }
                    if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                        //phone exists
                        phoneNumber.setText(dataSnapshot.child("phone-number").getValue(String.class));
                    }
                    if(dataSnapshot.child("workID").getValue(String.class) != null){
                        workId.setText(dataSnapshot.child("workID").getValue(String.class));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //do nothing
                }
            });
            imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    image.setImageBitmap(bmp);
                }
            });
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullEmployeeDetailFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }



    }
}
