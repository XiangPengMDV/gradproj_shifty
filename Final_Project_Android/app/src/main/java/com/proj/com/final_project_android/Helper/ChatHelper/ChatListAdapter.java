package com.proj.com.final_project_android.Helper.ChatHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.proj.com.final_project_android.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class ChatListAdapter extends BaseAdapter {

    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<ChatListItem> chat_list;

    public ChatListAdapter(Context context, ArrayList<ChatListItem> chat_list) {
        this.context = context;
        this.chat_list = chat_list;
    }

    @Override
    public int getCount() {
        return (chat_list != null) ? chat_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return chat_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.chat_list_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(chat_list.get(i).getName());
        holder.date.setText(chat_list.get(i).getDate());
        holder.message.setText(chat_list.get(i).getMessage());
        Picasso.with(context).load(chat_list.get(i).getImage()).into(holder.image);
        return view;
    }

    static class ViewHolder {
        final ImageView image;
        final TextView name;
        final TextView message;
        final TextView date;

        ViewHolder(View v) {
            image = (ImageView) v.findViewById(R.id.chat_list_image);
            name = (TextView) v.findViewById(R.id.chat_list_name);
            message = (TextView) v.findViewById(R.id.chat_list_message);
            date = (TextView) v.findViewById(R.id.chat_list_date);
        }
    }
}
