package com.proj.com.final_project_android.Helper.ChatHelper;


public class CurrentChatItem {
    String message;
    String currentUUID;
    String receiverUUID;

    public CurrentChatItem(String message, String currentUUID, String receiverUUID) {
        this.message = message;
        this.currentUUID = currentUUID;
        this.receiverUUID = receiverUUID;
    }

    public String getMessage() {
        return message;
    }

    public String getCurrentUUID() {
        return currentUUID;
    }

    public String getReceiverUUID() {
        return receiverUUID;
    }
}
