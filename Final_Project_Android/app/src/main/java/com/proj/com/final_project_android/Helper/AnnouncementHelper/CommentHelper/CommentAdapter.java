package com.proj.com.final_project_android.Helper.AnnouncementHelper.CommentHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.proj.com.final_project_android.R;

import java.util.ArrayList;

public class CommentAdapter extends BaseAdapter {
    private static final int ID_CONSTANT = 0x0001001;
    private final Context context;
    private final ArrayList<CommentItem> comment_list;

    public CommentAdapter(Context context, ArrayList<CommentItem> comment_list) {
        this.context = context;
        this.comment_list = comment_list;
    }

    @Override
    public int getCount() {
        return (comment_list != null) ? comment_list.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return comment_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ID_CONSTANT + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(
                            R.layout.comment_list_view_item,
                            viewGroup,
                            false
                    );
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(comment_list.get(i).getName());
        holder.comment.setText(comment_list.get(i).getComment());
        holder.date.setText(comment_list.get(i).getDate());


        return view;
    }

    static class ViewHolder {
        //final ImageView image;
        final TextView name;
        final TextView comment;
        final TextView date;


        ViewHolder(View v) {
            //image = (ImageView) v.findViewById(R.id.employee_item_image);
            name = (TextView) v.findViewById(R.id.comment_name);
            comment = (TextView) v.findViewById(R.id.comment_body);
            date = (TextView) v.findViewById(R.id.comment_date);
        }
    }
}
