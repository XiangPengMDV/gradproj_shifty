package com.proj.com.final_project_android.Helper.ChatHelper;

import android.support.annotation.Nullable;


public class ChatListItem {
    String name;
    String image;
    String uuid;
    String message;
    String date;
    String receiverID;

    public ChatListItem(String name, @Nullable String image, String message, String uuid, String receiverID) {
        this.name = name;
        this.image = image;
        this.uuid = uuid;
        this.message = message;
        this.receiverID = receiverID;
        this.date = date;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUuid() {
        return uuid;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }
}
