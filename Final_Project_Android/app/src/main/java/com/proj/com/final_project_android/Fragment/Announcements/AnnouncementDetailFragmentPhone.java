package com.proj.com.final_project_android.Fragment.Announcements;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.proj.com.final_project_android.Helper.AnnouncementHelper.CommentHelper.CommentAdapter;
import com.proj.com.final_project_android.Helper.AnnouncementHelper.CommentHelper.CommentItem;
import com.proj.com.final_project_android.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

public class AnnouncementDetailFragmentPhone extends Fragment {

    private static final String TAG = "AnnouncementDetailFragm";
    TextView title;
    TextView body;
    TextView author;
    TextView date;
    ListView commentList;
    String uuid;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ArrayList<CommentItem> mList;
    CommentAdapter mAdapter;
    Calendar mCal;


    //overrides
    public static AnnouncementDetailFragmentPhone newInstance() {

        Bundle args = new Bundle();

        AnnouncementDetailFragmentPhone fragment = new AnnouncementDetailFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCal = Calendar.getInstance();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.add_comment_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.announcement_detail_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        title = (TextView) getActivity().findViewById(R.id.announcement_detail_title);
        body = (TextView) getActivity().findViewById(R.id.announcement_detail_body);
        author = (TextView) getActivity().findViewById(R.id.announcement_detail_author);
        date = (TextView) getActivity().findViewById(R.id.announcement_detail_date);
        commentList = (ListView) getActivity().findViewById(R.id.announcements_comment_list);
        uuid = getArguments().getString("announcementID");
        mCompanyId = getArguments().getString("announcementCompanyStringDetail");
        mList = new ArrayList<>();
        mAdapter = new CommentAdapter(getActivity(), mList);
        commentList.setAdapter(mAdapter);
        pullAnnouncementDetail();
    }

    //Firebase
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {ConnectivityManager cm =
            (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            //network on
            if(item.getItemId() == R.id.add_comment_menu){
                AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
                LayoutInflater customInflator = getActivity().getLayoutInflater();
                final View dialogView = customInflator.inflate(R.layout.edit_profile_change_password_dialog, null);
                newPasswordDialogBuilder.setView(dialogView);
                final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
                newPasswordET.setInputType(InputType.TYPE_CLASS_TEXT);
                newPasswordDialogBuilder.setTitle("Add Comment");
                newPasswordDialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(newPasswordET.getText().toString().isEmpty()){
                            Toast.makeText(getContext(), "Comment was empty", Toast.LENGTH_SHORT).show();
                        } else {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Companies").child(mCompanyId)
                                    .child("announcements").child(uuid).child("comments").child(UUID.randomUUID().toString());
                            ref.child("comment").setValue(newPasswordET.getText().toString());
                            ref.child("authorID").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                            String date = dateFormat.format(mCal.getTime());
                            ref.child("date-posted").setValue(date);
                            Toast.makeText(getContext(), "Comment Posted", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog mainAlert = newPasswordDialogBuilder.create();
                mainAlert.show();
            }

        } else {
            //network off
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void pullAnnouncementDetail(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            //network
            Log.d(TAG, "pullAnnouncementDetail: " + uuid + mCompanyId);
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements").child(uuid);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    title.setText(dataSnapshot.child("title").getValue(String.class));
                    body.setText(dataSnapshot.child("body").getValue(String.class));
                    author.setText(dataSnapshot.child("author").getValue(String.class));
                    date.setText(dataSnapshot.child("date").getValue(String.class));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });



            final DatabaseReference commentRef = database.getReference().child("Companies")
                    .child(mCompanyId).child("announcements").child(uuid).child("comments");
            commentRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mList.add(new CommentItem(dataSnapshot.getKey(), dataSnapshot.child("name").getValue(String.class)
                            , dataSnapshot.child("comment").getValue(String.class)
                            , dataSnapshot.child("date-posted").getValue(String.class)));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else {
            //no network
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullAnnouncementDetail();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }







    }



}
