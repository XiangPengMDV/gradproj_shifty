package com.proj.com.final_project_android.Fragment.Schedule;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.Helper.ScheduleHelper.DailyScheduleItem;
import com.proj.com.final_project_android.Helper.ScheduleHelper.MyScheduleAdapter;
import com.proj.com.final_project_android.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class ScheduleFragmentPhone extends Fragment{
    private ListView scheduleList;
    private  ArrayList<Object> listMain;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private MyScheduleAdapter mAdapter;
    String mCompanyId;
    private static final String TAG = "ScheduleFragmentPhone";

    //Overrides
    public static ScheduleFragmentPhone newInstance() {
        Bundle args = new Bundle();
        ScheduleFragmentPhone fragment = new ScheduleFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.new_message_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(true);
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        //menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(false);
        menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.time_off_menu){
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    TimeOffRequestFragment.newInstance()
            ).addToBackStack(null).commit();
        }
        return super.onOptionsItemSelected(item);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("My Weekly Schedule");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("My Weekly Schedule");
        }
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        scheduleList = (ListView) getActivity().findViewById(R.id.my_schedule_list);
        listMain = new ArrayList<>();
        mAdapter = new MyScheduleAdapter(getActivity(), listMain);
        scheduleList.setAdapter(mAdapter);
        pullFromFirebase();
        super.onActivityCreated(savedInstanceState);
    }

    //Firebase
    public void pullFromFirebase(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            int delta;
            //listMain = new ArrayList<>();
            final Calendar now = Calendar.getInstance();
            final SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
            delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2;
            now.add(Calendar.DAY_OF_MONTH, delta);
            final String monday = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
            final String tuesday = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
            final String wednesday = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
            final String thursday = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
            final String friday = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
            final String saturday = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
            final String sunday = format.format(now.getTime());

            //monday
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("schedule");
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(monday);
                    if(dataSnapshot.child(monday) != null){
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(monday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(monday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(monday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(monday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(monday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //tuesday
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(tuesday);
                    if(dataSnapshot.child(tuesday) != null){
                        Log.d(TAG, "onDataChange: ");
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(tuesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(tuesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(tuesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(tuesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(tuesday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //wed
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(wednesday);
                    if(dataSnapshot.child(wednesday) != null){
                        Log.d(TAG, "onDataChange: ");
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(wednesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(wednesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(wednesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(wednesday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(wednesday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //thurs
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(thursday);
                    if(dataSnapshot.child(thursday) != null){
                        Log.d(TAG, "onDataChange: ");
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(thursday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(thursday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(thursday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(thursday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(thursday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //fri
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(friday);
                    if(dataSnapshot.child(friday) != null){
                        Log.d(TAG, "onDataChange: ");
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(friday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(friday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(friday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(friday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(friday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //saturday
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(saturday);
                    if(dataSnapshot.child(saturday) != null){
                        Log.d(TAG, "onDataChange: ");
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(saturday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(saturday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(saturday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(saturday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(saturday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //sun
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listMain.add(sunday);
                    if(dataSnapshot.child(sunday) != null){
                        Log.d(TAG, "onDataChange: ");
                        listMain.add(new DailyScheduleItem(dataSnapshot.child(sunday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("image").getValue(String.class)
                                , dataSnapshot.child(sunday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("name").getValue(String.class)
                                , dataSnapshot.child(sunday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-start").getValue(String.class) + "-" + dataSnapshot.child(sunday).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("shift-end").getValue(String.class)
                                , dataSnapshot.child(sunday).child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullFromFirebase();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }


}
