package com.proj.com.final_project_android.Fragment.Start;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.R;

public class StartFragment extends Fragment implements View.OnClickListener {


    private FirebaseAuth mAuth;


    EditText emailET;
    EditText passwordET;
    Button loginB;
    Button signUpB;
    TextView forgotPasswordTV;


    public static StartFragment newInstance() {
        Bundle args = new Bundle();
        StartFragment fragment = new StartFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        emailET = (EditText) getActivity().findViewById(R.id.email_input_login);
        passwordET = (EditText) getActivity().findViewById(R.id.password_input_login);
        loginB = (Button) getActivity().findViewById(R.id.login_button);
        loginB.setOnClickListener(this);
        signUpB = (Button) getActivity().findViewById(R.id.signup_button);
        signUpB.setOnClickListener(this);
        forgotPasswordTV = (TextView) getActivity().findViewById(R.id.forgot_password_text);
        forgotPasswordTV.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }

    //Onclick for button and forgotpassword text
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:
                if(signInValidation()){
                    loginWithFirebase(emailET.getText().toString().trim(), passwordET.getText().toString().trim());
                }
                break;
            case R.id.signup_button:
                //Send to register with back button capability
                getFragmentManager().beginTransaction().replace(
                        R.id.start_activity_container,
                        RegisterFragment.newInstance()
                ).addToBackStack(null).commit();
                break;
            case R.id.forgot_password_text:
                showForgotPasswordDialog();
                break;

        }
    }

    private void loginWithFirebase(String email, final String password){
        loginB.setEnabled(false);
        mAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(getActivity(), new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                SharedPreferences pref =  getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(getString(R.string.user_password_key), password);
                editor.apply();
                //login success
                //Send to coreActivity
                if(checkForDevice()){
                    //user exists meaning logged in
                    Intent coreIntent = new Intent();
                    coreIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    coreIntent.setClass(getActivity(), TabletCoreActivity.class);
                    startActivity(coreIntent);
                    getActivity().finish();
                } else {
                    //user exists meaning logged in
                    Intent coreIntent = new Intent();
                    coreIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    coreIntent.setClass(getActivity(), CoreActivity.class);
                    startActivity(coreIntent);
                    getActivity().finish();
                }
            }
        }).addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //login failure (wrong credentials)
                loginB.setEnabled(true);
                Toast.makeText(getContext(), "Wrong Credentials", Toast.LENGTH_LONG).show();
            }
        });
    }

    public Boolean checkForDevice(){
        //boolean determining tablet or not
        return getResources().getBoolean(R.bool.isTablet);
    }
    private Boolean signInValidation(){
            //one of the fields are empty
            if (emailET.getText().toString().isEmpty()){
                //email is empty
                Toast.makeText(getContext(), "Email Field Empty", Toast.LENGTH_SHORT).show();
                return false;
            } else if (passwordET.getText().toString().isEmpty() && !(emailET.getText().toString().isEmpty())) {
                //email is not empty but password is
                Toast.makeText(getContext(), "Password Field Empty", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                //both field are filled in
                if(Patterns.EMAIL_ADDRESS.matcher(emailET.getText().toString()).matches()){
                    //Proper Email Was Entered
                    //Assuming proper password will be entered because something has been entered
                    return true;
                } else {
                    //Non proper email entered
                    Toast.makeText(getContext(), "Please Enter a Proper Email", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
    }

    private void showForgotPasswordDialog(){
        AlertDialog.Builder passwordDialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater customInflator = getActivity().getLayoutInflater();
        final View dialogView = customInflator.inflate(R.layout.profile_forgot_password_dialog, null);
        passwordDialogBuilder.setView(dialogView);
        final EditText emailEditText = (EditText) dialogView.findViewById(R.id.email_dialog_input);
        passwordDialogBuilder.setTitle("Forgot Your Password?");
        passwordDialogBuilder.setMessage("Please Enter your email so we can send you a reset password email.");
        passwordDialogBuilder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //send password reset email from firebase
                if(emailEditText.getText().toString().isEmpty()){
                    //is empty
                    Toast.makeText(getContext(), "No Email Entered", Toast.LENGTH_LONG).show();
                } else {
                    mAuth.sendPasswordResetEmail(emailEditText.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getContext(), "Email Sent!", Toast.LENGTH_LONG).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //Failure to send email
                            Toast.makeText(getContext(), "Email doesn't exist in the database", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });
        passwordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //do nothing just go back
            }
        });
        AlertDialog mainAlert = passwordDialogBuilder.create();
        mainAlert.show();
    }


}
