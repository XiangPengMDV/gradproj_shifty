package com.proj.com.final_project_android.Fragment.Schedule;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.proj.com.final_project_android.Activity.CoreActivity;
import com.proj.com.final_project_android.Activity.TabletCoreActivity;
import com.proj.com.final_project_android.Helper.ScheduleHelper.SelectedDayAdapter;
import com.proj.com.final_project_android.Helper.ScheduleHelper.SelectedDayItem;
import com.proj.com.final_project_android.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

public class ScheduleFragment extends Fragment implements CalendarView.OnDateChangeListener, AdapterView.OnItemClickListener {

    private static final String TAG = "ScheduleFragment";
    private ArrayList<SelectedDayItem> mList;
    private ArrayList<SelectedDayItem> deleteList;
    private ArrayList<String> uuidsToNotify;
    private SelectedDayAdapter mAdapter;
    Boolean mEdit = false;
    String mCompanyId;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    CalendarView mCalendarView;
    TextView selectedDayTV;
    Calendar mCalendar;
    Menu mMenu;
    ListView selectedList;

    public static ScheduleFragment newInstance() {
        Bundle args = new Bundle();
        ScheduleFragment fragment = new ScheduleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.new_message_menu).setVisible(false);
        menu.findItem(R.id.add_schedule_menu).setVisible(true);
        menu.findItem(R.id.edit_schedule_menu).setVisible(true);
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        //menu.findItem(R.id.edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.add_comment_menu).setVisible(false);
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        //menu.findItem(R.id.add_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(false);
        menu.findItem(R.id.time_off_menu).setVisible(false);
        menu.findItem(R.id.send_time_off_menu).setVisible(false);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(false);
        menu.findItem(R.id.announcement_edit_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_schedule_menu:
                showAddSchedule();
                break;
            case R.id.edit_schedule_menu:
                editSchedule();
                break;
            case R.id.cancel_edit_schedule_menu:
                cancelEditSchedule();
                break;
            case R.id.save_edit_schedule_menu:
                deleteSelectedSchedule();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_fragment_tablet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Schedule");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Schedule");
        }
        mCalendarView = (CalendarView) getActivity().findViewById(R.id.schedule_calendar);
        selectedDayTV = (TextView) getActivity().findViewById(R.id.selected_day_text);
        selectedList = (ListView) getActivity().findViewById(R.id.selected_day_list);
        deleteList = new ArrayList<>();
        mList = new ArrayList<>();
        mAdapter = new SelectedDayAdapter(getActivity(), mList, mEdit);
        selectedList.setAdapter(mAdapter);
        mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(mCalendarView.getDate());
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        SimpleDateFormat weekDayFormat = new SimpleDateFormat("EEEE", Locale.US);
        selectedDayTV.setText(weekDayFormat.format(mCalendar.getTime()));
        selectedList.setOnItemClickListener(this);
        mCalendarView.setOnDateChangeListener(this);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        pullSelectedDayFromFirebase(dateFormat.format(mCalendar.getTime()));
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mEdit){
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.schedule_selected_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);

                //mList.add(deleteArray.get(i));
                deleteList.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteList.add(mList.get(i));
                //mList.remove(deleteArray.get(i));
            }
        }
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        SimpleDateFormat weekDayFormat = new SimpleDateFormat("EEEE", Locale.US);
        mCalendar.set(i, i1, i2);
        selectedDayTV.setText(weekDayFormat.format(mCalendar.getTime()));
        Log.d(TAG, "onSelectedDayChange: " + dateFormat.format(mCalendar.getTime()));
        mList.clear();
        pullSelectedDayFromFirebase(dateFormat.format(mCalendar.getTime()));
        //calendarView.setDate(i,i1,i2);
    }

    public void editSchedule(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Edit Schedule");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Edit Schedule");
        }
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.add_schedule_menu).setVisible(false);
        mMenu.findItem(R.id.edit_schedule_menu).setVisible(false);
        mMenu.findItem(R.id.save_edit_schedule_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_edit_schedule_menu).setVisible(true);
    }
    public void cancelEditSchedule(){
        if(getResources().getBoolean(R.bool.isTablet)){
            ((TabletCoreActivity)getActivity()).setTheDamnTitle("Schedule");
        } else {
            ((CoreActivity)getActivity()).setTheDamnTitle("Schedule");
        }
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.add_schedule_menu).setVisible(true);
        mMenu.findItem(R.id.edit_schedule_menu).setVisible(true);
        mMenu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
    }
    public void showAddSchedule(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AddScheduleFragment.newInstance()
        ).addToBackStack(null).commit();
    }




    //Firebase
    public void pullSelectedDayFromFirebase(final String selectedDay){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("schedule").child(selectedDay);
            ref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if(dataSnapshot != null){
                        mList.add(new SelectedDayItem(dataSnapshot.child("name").getValue(String.class)
                                , dataSnapshot.child("shift-start").getValue(String.class) + "-" + dataSnapshot.child("shift-end").getValue(String.class)
                                , dataSnapshot.child("image").getValue(String.class)
                                , dataSnapshot.getKey(),
                                dataSnapshot.child("uuid").getValue(String.class)));
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            pullSelectedDayFromFirebase(selectedDay);
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }


    }
    public void deleteSelectedSchedule(){
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
            final String daySelect = dateFormat.format(mCalendar.getTime());
            for(SelectedDayItem day: deleteList){
                Log.d(TAG, "deleteSelectedSchedule: " + day.getDateID() + daySelect);
                final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Companies")
                        .child(mCompanyId).child("schedule").child(daySelect).child(day.getDateID());

                ref.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: ");
                        mEdit = false;
                        mAdapter.updateEdit(false);
                        mMenu.findItem(R.id.add_schedule_menu).setVisible(true);
                        mMenu.findItem(R.id.edit_schedule_menu).setVisible(true);
                        mMenu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
                        mMenu.findItem(R.id.cancel_edit_schedule_menu).setVisible(false);
                    }
                });

                updateToNotification(day.getUuid(), daySelect);


            }
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    ScheduleFragment.newInstance()
            ).commit();
        } else {
            //no network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("No Network Found");
            alertDialog.setMessage("A network or wifi connection is required to pull your profile information. Please turn on your network and hit OK or CLOSE the app.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteSelectedSchedule();
                        }
                    });
            alertDialog.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }

    public void updateToNotification(String uuid, String date){
        if(uuid != null && date != null) {
            DatabaseReference notificationRef = database.getReference().child("Companies")
                    .child(mCompanyId).child("employees")
                    .child(uuid).child("notifications").child(UUID.randomUUID().toString());


            notificationRef.child("notificationText").setValue("You have been deleted to the schedule for " + date);
            notificationRef.child("read").setValue(false);
            notificationRef.child("notified").setValue(false);
        }

    }

}
